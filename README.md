# Kontorol Client Generator
The code in this repo is used to auto generate the Kontorol client libraries for each supported language.

[![License](https://img.shields.io/badge/license-AGPLv3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0.html)

## Deployment Instructions
The list of supported clients is [here](config/generator.all.ini)

Download the API scheme XML from http://www.kontorol.com/api_v3/api_schema.php.

To generate one client run:
```
$ php /opt/kontorol/clients-generator/exec.php -x/path-to-xml/KontorolClient.xml $CLIENT_NAME
```

For example, to generate a `php53` client run:
```
php /opt/kontorol/clients-generator/exec.php -x/path-to-xml/KontorolClient.xml php53
```

To generate all available clients, run:
```
while read CLIENT;do php /opt/kontorol/clients-generator/exec.php -x/path-to-xml/KontorolClient.xml $CLIENT;done < /opt/kontorol/clients-generator/config/generator.all.ini
```

## Getting started with the API
To learn how to use the Kontorol API, go to [developer.kontorol.com](https://developer.kontorol.com/)

## How you can help (guidelines for contributors) 
Thank you for helping Kontorol grow! If you'd like to contribute please follow these steps:
* Use the repository issues tracker to report bugs or feature requests
* Read [Contributing Code to the Kontorol Platform](https://github.com/kontorol/platform-install-packages/blob/master/doc/Contributing-to-the-Kontorol-Platform.md)
* Sign the [Kontorol Contributor License Agreement](https://agentcontribs.kontorol.org/)

## Where to get help
* Join the [Kontorol Community Forums](https://forum.kontorol.org/) to ask questions or start discussions
* Read the [Code of conduct](https://forum.kontorol.org/faq) and be patient and respectful

## Get in touch
You can learn more about Kontorol and start a free trial at: http://corp.kontorol.com    
Contact us via Twitter [@Kontorol](https://twitter.com/Kontorol) or email: community@kontorol.com  
We'd love to hear from you!

## License and Copyright Information
All code in this project is released under the [AGPLv3 license](http://www.gnu.org/licenses/agpl-3.0.html) unless a different license for a particular library is specified in the applicable library path.   

Copyright © Kontorol Inc. All rights reserved.   
Authors and contributors: See [GitHub contributors list](https://github.com/kontorol/clients-generator/graphs/contributors).  
