<?php

class NGXClientGenerator extends TypescriptClientGenerator
{
	function __construct($xmlPath, Zend_Config $config)
	{
		parent::__construct($xmlPath, $config,"ngx", "projects/kontorol-ngx-client/src/lib/api");
	}
}
