import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Inject, Injectable, Optional, Self } from '@angular/core';


import { HttpClient } from '@angular/common/http';
import { KontorolRequest } from './api/kontorol-request';
import { _FORMAT_TYPES_TOKEN_ } from './api/types/_FORMAT_TYPES_TOKEN_';
import { KontorolMultiRequest } from './api/kontorol-multi-request';
import { KontorolMultiResponse } from './api/kontorol-multi-response';
import { KontorolFileRequest } from './api/kontorol-file-request';
import { KontorolUploadRequest } from './api/kontorol-upload-request';
import { KontorolRequestAdapter } from './adapters/kontorol-request-adapter';
import { KontorolFileRequestAdapter } from './adapters/kontorol-file-request-adapter';
import { KontorolClientOptions, KONTOROL_CLIENT_OPTIONS } from './kontorol-client-options';
import { KontorolMultiRequestAdapter } from './adapters/kontorol-multi-request-adapter';
import { KontorolClientException } from './api/kontorol-client-exception';
import { KontorolUploadRequestAdapter } from './adapters/kontorol-upload-request-adapter';
import {
  KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS, KontorolRequestOptions,
  KontorolRequestOptionsArgs
} from './api/kontorol-request-options';

@Injectable()
export class KontorolClient {

  private _defaultRequestOptions: KontorolRequestOptions;

  constructor(private _http: HttpClient, @Inject(KONTOROL_CLIENT_OPTIONS) @Optional() @Self() private _options: KontorolClientOptions,
              @Inject(KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS) @Optional() @Self()  defaultRequestOptionsArgs: KontorolRequestOptionsArgs) {
    this._defaultRequestOptions = new KontorolRequestOptions(defaultRequestOptionsArgs || {});
  }

  public appendOptions(options: KontorolClientOptions): void {
    if (!options) {
      throw new KontorolClientException('client::append_options',`missing required argument 'options'`);
    }

    this._options = Object.assign(
      this._options || {}, options
    );
  }

  public setOptions(options: KontorolClientOptions): void {
    if (!options) {
      throw new KontorolClientException('client::set_options',`missing required argument 'options'`);
    }

    this._options = options;
  }

  public appendDefaultRequestOptions(args: KontorolRequestOptionsArgs): void {
    if (!args) {
      throw new KontorolClientException('client::append_default_request_options',`missing required argument 'args'`);
    }

    this._defaultRequestOptions = Object.assign(
      this._defaultRequestOptions || new KontorolRequestOptions(), new KontorolRequestOptions(args)
    );
  }

  public setDefaultRequestOptions(args: KontorolRequestOptionsArgs): void {
    if (!args) {
      throw new KontorolClientException('client::set_default_request_options',`missing required argument 'args'`);
    }

    this._defaultRequestOptions = new KontorolRequestOptions(args);
  }

  private _validateOptions(): Error | null {
    if (!this._options) {
      return new KontorolClientException('client::missing_options','cannot transmit request, missing client options (did you forgot to provide options manually or using KONTOROL_CLIENT_OPTIONS?)');
    }

    if (!this._options.endpointUrl) {
      return new KontorolClientException('client::missing_options', `cannot transmit request, missing 'endpointUrl' in client options`);
    }

    if (!this._options.clientTag) {
      return new KontorolClientException('client::missing_options', `cannot transmit request, missing 'clientTag' in client options`);
    }

    return null;
  }

  public request<T>(request: KontorolRequest<T>): Observable<T>;
  public request<T>(request: KontorolFileRequest): Observable<{ url: string }>;
  public request<T>(request: KontorolRequest<any>, format: _FORMAT_TYPES_TOKEN_, responseType: 'blob' | 'text'): Observable<any>;
  public request<T>(request: KontorolRequest<T> | KontorolFileRequest, format?: _FORMAT_TYPES_TOKEN_, responseType?: 'blob' | 'text'): Observable<T | { url: string }> {

    const optionsViolationError = this._validateOptions();

    if (optionsViolationError) {
      return observableThrowError(optionsViolationError);
    }

    if (typeof format !== 'undefined') {
      return new KontorolRequestAdapter(this._http).transmit(request, this._options, this._defaultRequestOptions, format +'', responseType);
    }

    if (request instanceof KontorolFileRequest) {
      return new KontorolFileRequestAdapter().transmit(request, this._options, this._defaultRequestOptions);

    } else if (request instanceof KontorolUploadRequest) {
      return new KontorolUploadRequestAdapter(this._options, this._defaultRequestOptions).transmit(request);
    }
    else if (request instanceof KontorolRequest) {
      return new KontorolRequestAdapter(this._http).transmit(request, this._options, this._defaultRequestOptions);
    } else {
      return observableThrowError(new KontorolClientException("client::request_type_error", 'unsupported request type requested'));
    }
  }

  public multiRequest(requests: KontorolRequest<any>[]): Observable<KontorolMultiResponse>
  public multiRequest(request: KontorolMultiRequest): Observable<KontorolMultiResponse>;
  public multiRequest(arg: KontorolMultiRequest | KontorolRequest<any>[]): Observable<KontorolMultiResponse> {
    const optionsViolationError = this._validateOptions();
    if (optionsViolationError) {
      return observableThrowError(optionsViolationError);
    }

    const request = arg instanceof KontorolMultiRequest ? arg : (arg instanceof Array ? new KontorolMultiRequest(...arg) : null);
    if (!request) {
      return observableThrowError(new KontorolClientException('client::invalid_request', `Expected argument of type Array or KontorolMultiRequest`));
    }

    const containsFileRequest = request.requests.some(item => item instanceof KontorolFileRequest);
    if (containsFileRequest) {
      return observableThrowError(new KontorolClientException('client::invalid_request', `multi-request not support requests of type 'KontorolFileRequest', use regular request instead`));
    } else {
      return new KontorolMultiRequestAdapter(this._http).transmit(request, this._options, this._defaultRequestOptions);
    }
  }
}
