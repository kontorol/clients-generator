# KontorolClient

[![CI Status](http://img.shields.io/travis/kontorol/KontorolGeneratedAPIClientsSwift.svg?style=flat)](https://travis-ci.org/kontorol/KontorolGeneratedAPIClientsSwift)
[![Version](https://img.shields.io/cocoapods/v/KontorolClient.svg?style=flat)](http://cocoapods.org/pods/KontorolClient)
[![License](https://img.shields.io/cocoapods/l/KontorolClient.svg?style=flat)](http://cocoapods.org/pods/KontorolClient)
[![Platform](https://img.shields.io/cocoapods/p/KontorolClient.svg?style=flat)](http://cocoapods.org/pods/KontorolClient)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KontorolClient is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KontorolClient"
```

## Author

community@kontorol.com

## License and Copyright Information
All code in this project is released under the [AGPLv3 license](http://www.gnu.org/licenses/agpl-3.0.html) unless a different license for a particular library is specified in the applicable library path.   

Copyright © Kontorol Inc. All rights reserved.   
Authors and contributors: See [GitHub contributors list](https://github.com/kontorol/pakhshkit-ios-samples/graphs/contributors).  

## Publishing

```ruby
pod lib lint --fail-fast --allow-warnings
pod trunk push KontorolClient.podspec --allow-warnings 
```
