<?php
/* set this path to the location of Zend/Loader/StandardAutoloader.php, 
 * the client library can be used with any other php5.3 namespace style autoloaders (for example symfony2 & doctrine2)
*/
define('CONFIG_FILE', 'config.ini');

use Kontorol\Client\Configuration as KontorolConfiguration;
use Kontorol\Client\Client as KontorolClient;
use Kontorol\Client\Enum\SessionType as KontorolSessionType;
use Kontorol\Client\ApiException;
use Kontorol\Client\ClientException;

// load zend framework 2
require_once(dirname(__FILE__).'/ClassLoader/ClassLoader.php');
$loader = new Symfony\Component\ClassLoader\ClassLoader();
// register Kontorol namespace
$loader->addPrefix('Kontorol', dirname(__FILE__).'/../library');
$loader->addPrefix('Test', dirname(__FILE__));
$loader->register();

$testerConfig = parse_ini_file(dirname(__FILE__).'/'.CONFIG_FILE);

// init kontorol configuration
$config = new KontorolConfiguration();
$config->setServiceUrl($testerConfig['serviceUrl']);
$config->setCurlTimeout(120);
$config->setLogger(new \Test\SampleLoggerImplementation());

// init kontorol client
$client = new KontorolClient($config);

// generate session
$ks = $client->generateSession($testerConfig['adminSecret'], $testerConfig['userId'], KontorolSessionType::ADMIN, $testerConfig['partnerId']);
$config->getLogger()->log('Kontorol session (ks) was generated successfully: ' . $ks);
$client->setKs($ks);

// check connectivity
try
{
	$client->getSystemService()->ping();
}
catch (ApiException $ex)
{
	$config->getLogger()->log('Ping failed with api error: '.$ex->getMessage());
	die;
}
catch (ClientException $ex)
{
	$config->getLogger()->log('Ping failed with client error: '.$ex->getMessage());
	die;
}

// run the tester
$tester = new \Test\Zend2ClientTester($client, intval($testerConfig['partnerId']));
$tester->run();
