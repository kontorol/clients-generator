/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kontorol.services;

import java.util.List;

import android.util.Log;

import com.kontorol.client.Client;
import com.kontorol.client.services.BaseEntryService;
import com.kontorol.client.services.FlavorAssetService;
import com.kontorol.client.types.APIException;
import com.kontorol.client.types.EntryContextDataParams;
import com.kontorol.client.types.EntryContextDataResult;
import com.kontorol.client.types.FilterPager;
import com.kontorol.client.types.FlavorAsset;
import com.kontorol.client.types.FlavorAssetFilter;
import com.kontorol.client.types.ListResponse;
import com.kontorol.client.utils.request.RequestBuilder;
import com.kontorol.client.utils.response.OnCompletion;
import com.kontorol.client.utils.response.base.Response;
import com.kontorol.utils.ApiHelper;

/**
 * Retrieve information and invoke actions on Flavor Asset
 */
public class FlavorAssets {

    
    /**
     * Return flavorAsset lists from getContextData call
     * @param TAG
     * @param entryId
     * @param flavorTags
     * @return
     * @throws APIException
     */
    public static void listAllFlavorsFromContext(String TAG, String entryId, String flavorTags, OnCompletion<Response<EntryContextDataResult>> onCompletion) throws APIException {
        EntryContextDataParams params = new EntryContextDataParams();
        params.setFlavorTags(flavorTags);
        BaseEntryService.GetContextDataBaseEntryBuilder requestBuilder = BaseEntryService.getContextData(entryId, params)
        .setCompletion(onCompletion);
    }
}
