// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================

package com.kontorol.client.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.kontorol.client.APIOkRequestsExecutor;
import com.kontorol.client.enums.EntryStatus;
import com.kontorol.client.enums.EntryType;
import com.kontorol.client.enums.MediaType;
import com.kontorol.client.enums.ModerationFlagType;
import com.kontorol.client.services.DataService;
import com.kontorol.client.services.DataService.AddDataBuilder;
import com.kontorol.client.services.FlavorAssetService;
import com.kontorol.client.services.FlavorAssetService.GetByEntryIdFlavorAssetBuilder;
import com.kontorol.client.services.MediaService;
import com.kontorol.client.services.MediaService.AddContentMediaBuilder;
import com.kontorol.client.services.MediaService.AddFromUploadedFileMediaBuilder;
import com.kontorol.client.services.MediaService.AddFromUrlMediaBuilder;
import com.kontorol.client.services.MediaService.AddMediaBuilder;
import com.kontorol.client.services.MediaService.CountMediaBuilder;
import com.kontorol.client.services.MediaService.GetMediaBuilder;
import com.kontorol.client.services.MediaService.ListFlagsMediaBuilder;
import com.kontorol.client.services.MediaService.ListMediaBuilder;
import com.kontorol.client.services.MediaService.UpdateMediaBuilder;
import com.kontorol.client.services.MediaService.UploadMediaBuilder;
import com.kontorol.client.services.PlaylistService;
import com.kontorol.client.services.PlaylistService.ExecuteFromFiltersPlaylistBuilder;
import com.kontorol.client.services.UploadTokenService;
import com.kontorol.client.services.UploadTokenService.AddUploadTokenBuilder;
import com.kontorol.client.services.UploadTokenService.UploadUploadTokenBuilder;
import com.kontorol.client.types.BaseEntry;
import com.kontorol.client.types.Partner;
import com.kontorol.client.types.DataEntry;
import com.kontorol.client.types.FlavorAsset;
import com.kontorol.client.types.ListResponse;
import com.kontorol.client.types.MediaEntry;
import com.kontorol.client.types.MediaEntryFilter;
import com.kontorol.client.types.MediaEntryFilterForPlaylist;
import com.kontorol.client.types.ModerationFlag;
import com.kontorol.client.types.UploadToken;
import com.kontorol.client.types.UploadedFileTokenResource;
import com.kontorol.client.utils.request.NullRequestBuilder;
import com.kontorol.client.utils.request.ServeRequestBuilder;
import com.kontorol.client.utils.response.OnCompletion;
import com.kontorol.client.utils.response.base.Response;

public class PartnerServiceTest extends BaseTest {

	public void testPartner() throws Exception {
		startAdminSession();

		final CountDownLatch doneSignal = new CountDownLatch(1);
		getTestPartner(new OnCompletion<Partner>() {

			@Override
			public void onComplete(Partner partner) {
				assertEquals(testConfig.getPartnerId(), (int) partner.getId());
				doneSignal.countDown();
			}
		});
		doneSignal.await();
	}
}
