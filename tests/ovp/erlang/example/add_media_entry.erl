#!/usr/bin/env escript
%% -*- erlang -*-
%%! -pa ./ebin -pa ./deps/jsx/ebin -pa ./deps/erlsom/ebin

-module(test1).
-import(io, [format/2]).

-include_lib("../src/kontorol_client.hrl").

main(_) ->
    application:start(inets),
    
    ClientConfiguration = #kontorol_configuration{
    	client_options = [{verbose, debug}]
    }, 
    ClientRequest = #kontorol_request{
    	ks = <<"KS Place Holder">>
    },
    Entry = #kontorol_media_entry{name = <<"test entry">>, mediaType = 2},
    Results = kontorol_media_service:add(ClientConfiguration, ClientRequest, Entry),

	io:format("Created entry: ~p~n", [Results]).
