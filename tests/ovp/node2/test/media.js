
const fs = require('fs');
const expect = require("chai").expect;
const kontorol = require('../KontorolClient');

const testConfig = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
const {secret, partnerId, serviceUrl} = testConfig;

let config = new kontorol.Configuration();
config.serviceUrl = serviceUrl;

const client = new kontorol.Client(config);


describe("Start session", () => {
    describe("User KS", () => {
    	let userId = null;
    	let type = kontorol.enums.SessionType.USER;
    	let expiry = null;
    	let privileges = null;

    	it('not null', (done) => {
    		kontorol.services.session.start(secret, userId, type, partnerId, expiry, privileges)
        	.completion((success, ks) => {
        		expect(success).to.equal(true);
        		expect(ks).to.not.be.a('null');
        		client.setKs(ks);
        		done();
        	})
        	.execute(client);
        });
    });
});

describe("Add media", () => {
    describe("Multiple requests", () => {

    	let entry = new kontorol.objects.MediaEntry({
    		mediaType: kontorol.enums.MediaType.VIDEO,
    		name: 'test'
    	});

    	let uploadToken = new kontorol.objects.UploadToken({
    	});

    	let createdEntry;
    	let createdUploadToken;

    	it('entry created', (done) => {
    		kontorol.services.media.add(entry)
    		.execute(client)
    		.then((entry) => {
        		expect(entry).to.not.be.a('null');
        		expect(entry.id).to.not.be.a('null');
        		expect(entry.status.toString()).to.equal(kontorol.enums.EntryStatus.NO_CONTENT);

        		createdEntry = entry;
        		return kontorol.services.uploadToken.add(uploadToken)
        		.execute(client);
    		})
    		.then((uploadToken) => {
        		expect(uploadToken).to.not.be.a('null');
        		expect(uploadToken.id).to.not.be.a('null');
        		expect(uploadToken.status).to.equal(kontorol.enums.UploadTokenStatus.PENDING);

        		createdUploadToken = uploadToken;
        		
        		let mediaResource = new kontorol.objects.UploadedFileTokenResource({
        			token: uploadToken.id
            	});
        		
        		return kontorol.services.media.addContent(createdEntry.id, mediaResource)
        		.execute(client);
    		})
    		.then((entry) => {
        		expect(entry.status.toString()).to.equal(kontorol.enums.EntryStatus.IMPORT);

        		let filePath = './test/DemoVideo.mp4';
        		return kontorol.services.uploadToken.upload(createdUploadToken.id, filePath)
        		.execute(client);
    		})
    		.then((uploadToken) => {
        		expect(uploadToken.status).to.equal(kontorol.enums.UploadTokenStatus.CLOSED);
        		done();
    		});
    	});
    });
    

    describe("Single multi-request", () => {
    	let entry = new kontorol.objects.MediaEntry({
    		mediaType: kontorol.enums.MediaType.VIDEO,
    		name: 'test'
    	});

    	let uploadToken = new kontorol.objects.UploadToken({
    	});

		let mediaResource = new kontorol.objects.UploadedFileTokenResource({
			token: '{2:result:id}'
    	});
		
		let filePath = './test/DemoVideo.mp4';

    	it('entry created', (done) => {
    		kontorol.services.media.add(entry)
    		.add(kontorol.services.uploadToken.add(uploadToken))
    		.add(kontorol.services.media.addContent('{1:result:id}', mediaResource))
    		.add(kontorol.services.uploadToken.upload('{2:result:id}', filePath))
    		.execute(client)
    		.then((results) => {
    			
    			entry = results[0];
        		expect(entry).to.not.be.a('null');
        		expect(entry.id).to.not.be.a('null');
        		expect(entry.status.toString()).to.equal(kontorol.enums.EntryStatus.NO_CONTENT);

    			uploadToken = results[1];
        		expect(uploadToken).to.not.be.a('null');
        		expect(uploadToken.id).to.not.be.a('null');
        		expect(uploadToken.status).to.equal(kontorol.enums.UploadTokenStatus.PENDING);

    			entry = results[2];
        		expect(entry.status.toString()).to.equal(kontorol.enums.EntryStatus.IMPORT);

    			uploadToken = results[3];
        		expect(uploadToken.status).to.equal(kontorol.enums.UploadTokenStatus.CLOSED);
        		
        		done();
    		});
    	});
    });

	describe("from buffers", () => {
		const filePath = './test/DemoVideo.mp4';
		const uploadToken = new kontorol.objects.UploadToken();
		let createdUploadToken;
		it('creates file', (done) => {
			kontorol.services.uploadToken.add(uploadToken)
				.execute(client)
				.then(token => {
					createdUploadToken = token;
					return kontorol.services.uploadToken.upload(createdUploadToken.id, Buffer.alloc(0), false, false, 0)
						.execute(client);
				})
				.then((uploadToken) => {
					expect(uploadToken.status).to.equal(kontorol.enums.UploadTokenStatus.PARTIAL_UPLOAD);
					const mediaEntry = new kontorol.objects.MediaEntry();
					mediaEntry.mediaType = kontorol.enums.MediaType.VIDEO;
					return kontorol.services.media.add(mediaEntry)
						.execute(client);
				})
				.then(entry => {
					const resource = new kontorol.objects.UploadedFileTokenResource();
					resource.token = createdUploadToken.id;
					return kontorol.services.media.addContent(entry.id, resource).execute(client);
				})
				.then(() => {
					return new Promise((resolve, reject) => {
						const uploads = [];
						const stats = fs.statSync(filePath);
						const rs = fs.createReadStream(filePath);
						let resumeAt = 0;
						rs.on('data', (data) => {
							const p = kontorol.services.uploadToken.upload(createdUploadToken.id, data, true, rs.bytesRead === stats.size, resumeAt)
								.execute(client);
							uploads.push(p);
							resumeAt += data.length;
						});
						rs.once('close', () => resolve(Promise.all(uploads)));
						rs.once('error', reject);
					})
				})
				.then(uploadTokens => {
					const lastUpload = uploadTokens.pop();
					uploadTokens.forEach((token) => expect(token.status).to.equal(kontorol.enums.UploadTokenStatus.PARTIAL_UPLOAD));
					expect(lastUpload.status).to.equal(kontorol.enums.UploadTokenStatus.CLOSED);
					done();
				})
				.catch((err) => done(err));
		});
	});
});
