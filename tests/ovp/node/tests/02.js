var Unit = require('deadunit')
var kc = require('../KontorolClient');
var ktypes = require('../KontorolTypes');
var vo = require ('../KontorolVO.js');
var config = require ('./config.js');

    var cb = function (results){
	if(results){
	    var test = Unit.test('Create -2 Admin session', function () {
	    this.count(1)
		if(results.code && results.message){
		    this.log(results.message);
		    this.log(results.code);
		    this.ok(false)
		}else{
		    this.log('KS is: '+results);
		    this.ok(true);
		}
		test.writeConsole() // writes colorful output!
	    }
	}else{
	    console.log('Something went wrong here :(');
	}
    }

    var kontorol_conf = new kc.KontorolConfiguration(config.minus2_partner_id);
    kontorol_conf.serviceUrl = config.service_url ;
    var client = new kc.KontorolClient(kontorol_conf);
    var type = ktypes.KontorolSessionType.ADMIN;

    var expiry = null;
    var privileges = null;
    var ks = client.session.start(cb, config.minus2_admin_secret, config.user_id, type, config.minus2_partner_id, expiry, privileges);
