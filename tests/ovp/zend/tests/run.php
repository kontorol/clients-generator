<?php

define('CONFIG_FILE', 'config.ini');

require_once(dirname(__FILE__).'/TestsAutoloader.php');
TestsAutoloader::register();

require_once(dirname(__FILE__).'/SampleLoggerImplementation.php');
require_once(dirname(__FILE__).'/Test/ZendClientTester.php');

$testerConfig = parse_ini_file(dirname(__FILE__).'/'.CONFIG_FILE);

// init kontorol configuration
$config = new Kontorol_Client_Configuration();
$config->serviceUrl = $testerConfig['serviceUrl'];
$config->curlTimeout = 120;
$config->setLogger(new SampleLoggerImplementation());

// init kontorol client
$client = new Kontorol_Client_Client($config);

// generate session
$ks = $client->generateSession($testerConfig['adminSecret'], $testerConfig['userId'], Kontorol_Client_Enum_SessionType::ADMIN, $testerConfig['partnerId']);
$config->getLogger()->log('Kontorol session (ks) was generated successfully: ' . $ks);
$client->setKs($ks);

// check connectivity
try
{
	$client->system->ping();
}
catch (Kontorol_Client_Exception $ex)
{
	$config->getLogger()->log('Ping failed with api error: '.$ex->getMessage());
	die;
}
catch (Kontorol_Client_ClientException $ex)
{
	$config->getLogger()->log('Ping failed with client error: '.$ex->getMessage());
	die;
}

// run the tester
$tester = new ZendClientTester($client, intval($testerConfig['partnerId']));
$tester->run();
