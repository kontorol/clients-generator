import { KontorolClient } from "../kontorol-client-service";
import { MediaListAction } from "../api/types/MediaListAction";
import { KontorolMediaListResponse } from "../api/types/KontorolMediaListResponse";
import { KontorolMediaEntry } from "../api/types/KontorolMediaEntry";
import { KontorolMediaType } from "../api/types/KontorolMediaType";
import { getClient } from "./utils";
import { LoggerSettings, LogLevels } from "../api/kontorol-logger";
import { asyncAssert } from "./utils";

describe(`service "Media" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return getClient()
      .then(client => {
        kontorolClient = client;
      }).catch(error => {
          // can do nothing since jasmine will ignore any exceptions thrown from before all
      });
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test(`invoke "list" action`, (done) => {

      if (!kontorolClient)
      {
          done.fail(`failure during 'SessionStart'. aborting test`);
          return;
      }

	  expect.assertions(5);
    kontorolClient.request(new MediaListAction()).then(
      (response) => {
	      asyncAssert(() => {
              expect(response instanceof KontorolMediaListResponse).toBeTruthy();
              expect(response.objects).toBeDefined();
              expect(response.objects instanceof Array).toBeTruthy();
              expect(response.objects.length).toBeGreaterThan(0);
              expect(response.objects[0] instanceof KontorolMediaEntry).toBeTruthy();
	      });

        done();
      },
      () => {
        done.fail(`failed to perform request`);
      }
    );
  });

  /*
    def test_createRemote(self):
        mediaEntry = KontorolMediaEntry()
        mediaEntry.setName("pytest.MediaTests.test_createRemote")
        mediaEntry.setMediaType(KontorolMediaType(KontorolMediaType.VIDEO))

        ulFile = getTestFile("DemoVideo.flv")
        uploadTokenId = self.client.media.upload(ulFile)

        mediaEntry = self.client.media.addFromUploadedFile(mediaEntry, uploadTokenId)

        self.assertIsInstance(mediaEntry.getId(), six.text_type)

        #cleanup
        self.client.media.delete(mediaEntry.id)
  */
  xtest(`invoke "createRemote" action`, () => {
    const media = new KontorolMediaEntry({
      name: "typescript.MediaTests.test_createRemote",
      mediaType: KontorolMediaType.video
    });
  });

  describe(`utf-8 tests`, () => {
    /*
      def test_utf8_name(self):
          test_unicode = six.u('\u03dd\xf5\xf6')  #an odd representation of the word 'FOO'
          mediaEntry = KontorolMediaEntry()
          mediaEntry.setName(u'pytest.MediaTests.test_UTF8_name'+test_unicode)
          mediaEntry.setMediaType(KontorolMediaType(KontorolMediaType.VIDEO))
          ulFile = getTestFile('DemoVideo.flv')
          uploadTokenId = self.client.media.upload(ulFile)

          #this will throw an exception if fail.
          mediaEntry = self.client.media.addFromUploadedFile(mediaEntry, uploadTokenId)

          self.addCleanup(self.client.media.delete, mediaEntry.getId())
     */
    xtest(`support utf-8 name`, () => {
      const media = new KontorolMediaEntry({
        name: "typescript.MediaTests.test_UTF8_name" + "\u03dd\xf5\xf6",
        mediaType: KontorolMediaType.video
      });
    });

    /*
      def test_utf8_tags(self):

          test_unicode = u'\u03dd\xf5\xf6'  #an odd representation of the word 'FOO'
          mediaEntry = KontorolMediaEntry()
          mediaEntry.setName('pytest.MediaTests.test_UTF8_tags')
          mediaEntry.setMediaType(KontorolMediaType(KontorolMediaType.VIDEO))
          ulFile = getTestFile('DemoVideo.flv')
          uploadTokenId = self.client.media.upload(ulFile)

          mediaEntry.setTags(test_unicode)

          #this will throw an exception if fail.
          mediaEntry = self.client.media.addFromUploadedFile(mediaEntry, uploadTokenId)

          self.addCleanup(self.client.media.delete, mediaEntry.getId())
     */
    xtest(`support utf-8 tags`, () => {
      const media = new KontorolMediaEntry({
        name: "typescript.MediaTests.test_UTF8_tags",
        mediaType: KontorolMediaType.video
      });
    });
  });
});
