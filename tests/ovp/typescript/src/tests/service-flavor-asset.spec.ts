import { KontorolClient } from "../kontorol-client-service";
import { FlavorAssetListAction } from "../api/types/FlavorAssetListAction";
import { BaseEntryListAction } from "../api/types/BaseEntryListAction";
import { KontorolFlavorAssetListResponse } from "../api/types/KontorolFlavorAssetListResponse";
import { KontorolMediaEntryFilter } from "../api/types/KontorolMediaEntryFilter";
import { KontorolFlavorAsset } from "../api/types/KontorolFlavorAsset";
import { KontorolFlavorAssetFilter } from "../api/types/KontorolFlavorAssetFilter";
import { getClient } from "./utils";
import { LoggerSettings, LogLevels } from "../api/kontorol-logger";
import { asyncAssert } from "./utils";
import { KontorolResponse } from '../api';

describe(`service "Flavor" tests`, () => {
	let kontorolClient: KontorolClient = null;

	beforeAll(async () => {
		LoggerSettings.logLevel = LogLevels.error; // suspend warnings

		return getClient()
			.then(client => {
				kontorolClient = client;
			}).catch(error => {
				// can do nothing since jasmine will ignore any exceptions thrown from before all
			});
	});

	afterAll(() => {
		kontorolClient = null;
	});

	test("flavor list", (done) => {

		expect.assertions(4);
		kontorolClient.multiRequest(
			[
				new BaseEntryListAction({
					filter: new KontorolMediaEntryFilter({
						flavorParamsIdsMatchOr: '0'
					})
				}),
				new FlavorAssetListAction(
					{
						filter: new KontorolFlavorAssetFilter(
							{
								entryIdEqual: ''
							}
						).setDependency(['entryIdEqual',0,'objects:0:id'])
					}
				)
			])
			.then(
				responses => {
					const response: KontorolResponse<KontorolFlavorAssetListResponse> = responses[1];
					asyncAssert(() => {
						expect(response.result instanceof KontorolFlavorAssetListResponse).toBeTruthy();
						if (response.result instanceof KontorolFlavorAssetListResponse) {
							expect(Array.isArray(response.result.objects)).toBeTruthy();
							expect(response.result.objects.length).toBeGreaterThan(0);
							expect(response.result.objects[0] instanceof KontorolFlavorAsset).toBeTruthy();
						}
					});
					done();
				},
				(error) => {
					done.fail(error);
				}
			);
	});
});
