import { KontorolClient } from "../kontorol-client-service";
import { PlaylistListAction } from "../api/types/PlaylistListAction";
import { KontorolPlaylistListResponse } from "../api/types/KontorolPlaylistListResponse";
import { KontorolPlaylist } from "../api/types/KontorolPlaylist";
import { KontorolPlaylistType } from "../api/types/KontorolPlaylistType";
import { PlaylistAddAction } from "../api/types/PlaylistAddAction";
import { PlaylistDeleteAction } from "../api/types/PlaylistDeleteAction";
import { PlaylistUpdateAction } from "../api/types/PlaylistUpdateAction";
import { getClient } from "./utils";
import { LoggerSettings, LogLevels } from "../api/kontorol-logger";
import { asyncAssert } from "./utils";

describe(`service "Playlist" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return getClient()
      .then(client => {
        kontorolClient = client;
      }).catch(error => {
        // can do nothing since jasmine will ignore any exceptions thrown from before all
      });
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test(`invoke "list" action`, (done) => {
	  expect.assertions(5);
    kontorolClient.request(new PlaylistListAction()).then(
      (response) => {
	      asyncAssert(() => {
		      expect(response instanceof KontorolPlaylistListResponse).toBeTruthy();
		      expect(response.objects).toBeDefined();
		      expect(response.objects instanceof Array).toBeTruthy();
		      expect(response.objects.length).toBeGreaterThan(0);
		      expect(response.objects[0] instanceof KontorolPlaylist).toBeTruthy();
	      });

        done();
      },
      () => {
        done.fail(`failed to perform request`);
      }
    );
  });

  test(`invoke "createRemote:staticList" action`, (done) => {
    const playlist = new KontorolPlaylist({
      name: "tstest.PlaylistTests.test_createRemote",
      playlistType: KontorolPlaylistType.staticList
    });
	  expect.assertions(2);
    kontorolClient.request(new PlaylistAddAction({ playlist }))
      .then(
        (response) => {
	        asyncAssert(() => {
		        expect(response instanceof KontorolPlaylist).toBeTruthy();
		        expect(typeof response.id).toBe("string");
	        });

	        kontorolClient.request(new PlaylistDeleteAction({ id: response.id })).then(
                () => {
	                done();
                },
                () => {
	                done();
                }
            );
        },
        (error) => {
          done.fail(error);
        }
      );
  });

  test(`invoke "update" action`, (done) => {
    const playlist = new KontorolPlaylist({
      name: "tstest.PlaylistTests.test_createRemote",
      referenceId: "tstest.PlaylistTests.test_update",
      playlistType: KontorolPlaylistType.staticList
    });
	  expect.assertions(1);
    kontorolClient.request(new PlaylistAddAction({ playlist }))
      .then(({ id }) => {
          playlist.name = "Changed!";
          return kontorolClient.request(new PlaylistUpdateAction({ id, playlist }));
        }
      )
      .then(({ id, name }) => {
	      asyncAssert(() => {
		      expect(name).toBe("Changed!");
	      });
            kontorolClient.request(new PlaylistDeleteAction({ id })).then(
	            () => {
		            done();
	            },
	            () => {
		            done();
	            });
      })
      .catch((error) => {
        done.fail(error);
      });
  });

  test(`invoke "createRemote:dynamicList" action`, (done) => {
    const playlist = new KontorolPlaylist({
      name: "tstest.PlaylistTests.test_createRemote",
      playlistType: KontorolPlaylistType.dynamic,
      totalResults: 0
    });
	  expect.assertions(2);
    kontorolClient.request(new PlaylistAddAction({ playlist }))
      .then(
        (response) => {
	        asyncAssert(() => {
		        expect(response instanceof KontorolPlaylist).toBeTruthy();
		        expect(typeof response.id).toBe("string");
	        });
          kontorolClient.request(new PlaylistDeleteAction({ id: response.id })).then(
	          () => {
		          done();
	          },
	          () => {
		          done();
	          }
          );
        },
        (error) => {
          done.fail(error);
        }
      );
  });
});
