import * as fs from "fs";
import * as path from "path";
import { TestsConfig } from "./tests-config";
import { KontorolClient } from "../kontorol-client-service";
import { SessionStartAction } from "../api/types/SessionStartAction";
import { KontorolSessionType } from "../api/types/KontorolSessionType";

export function getTestFile(): string | Buffer {
  return fs.readFileSync(path.join(__dirname, "DemoVideo.flv"));
}

export function asyncAssert(callback) {
	try {
		callback();
	} catch(e) {
		fail(e);
	}
}

export function escapeRegExp(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\$&");
}

export function getClient(): Promise<KontorolClient> {
    const httpConfiguration = {
        endpointUrl: TestsConfig.endpointUrl,
        clientTag: TestsConfig.clientTag
    };

    let client = new KontorolClient(httpConfiguration);


    return client.request(new SessionStartAction({
        secret: TestsConfig.adminSecret,
        userId: TestsConfig.userName,
        type: KontorolSessionType.admin,
        partnerId: <any>TestsConfig.partnerId * 1
    })).then(ks => {
        client.setDefaultRequestOptions({
            ks
        });
        return client;
    },
        error => {
            console.error(`failed to create session with the following error 'SessionStartAction'`);
            throw error;
        });
}
