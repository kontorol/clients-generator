import { KontorolClient } from "../kontorol-client-service";
import { UiConfListAction } from "../api/types/UiConfListAction";
import { KontorolUiConfListResponse } from "../api/types/KontorolUiConfListResponse";
import { KontorolUiConf } from "../api/types/KontorolUiConf";
import { KontorolUiConfFilter } from "../api/types/KontorolUiConfFilter";
import { KontorolUiConfObjType } from "../api/types/KontorolUiConfObjType";
import { UiConfListTemplatesAction } from "../api/types/UiConfListTemplatesAction";
import { getClient } from "./utils";
import { LoggerSettings, LogLevels } from "../api/kontorol-logger";
import { asyncAssert } from "./utils";

describe(`service "UIConf" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return getClient()
      .then(client => {
        kontorolClient = client;
      }).catch(error => {
        // can do nothing since jasmine will ignore any exceptions thrown from before all
      });
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test("uiconf list", (done) => {
	  expect.assertions(4);
    kontorolClient.request(new UiConfListAction())
      .then(
        response => {
	        asyncAssert(() => {
		        expect(response instanceof KontorolUiConfListResponse).toBeTruthy();
		        expect(Array.isArray(response.objects)).toBeTruthy();
		        expect(response.objects.length).toBeGreaterThan(0);
		        expect(response.objects[0] instanceof KontorolUiConf).toBeTruthy();
	        });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });


  // TODO [kmc] investigate response
  xtest("get players", (done) => {
    const players = [KontorolUiConfObjType.player, KontorolUiConfObjType.playerV3, KontorolUiConfObjType.playerSl];
    const filter = new KontorolUiConfFilter({ objTypeIn: players.join(",") });

	  expect.assertions(2);
    kontorolClient.request(new UiConfListAction(filter))
      .then(
        response => {
	        asyncAssert(() => {
		        expect(response.objects.length).toBeGreaterThan(0);
		        expect(players.indexOf(Number(response.objects[0].objType)) !== -1).toBeTruthy();
        });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });

  test("get video players", (done) => {
    const players = [KontorolUiConfObjType.player, KontorolUiConfObjType.playerV3, KontorolUiConfObjType.playerSl];
    const filter = new KontorolUiConfFilter({
      objTypeIn: players.join(","),
      tagsMultiLikeOr: "player"
    });

	  expect.assertions(1);
    kontorolClient.request(new UiConfListAction(filter))
      .then(
        response => {
	        expect(response.objects.length).toBeGreaterThan(0);
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });

  test("uiconf list templates", (done) => {
	  expect.assertions(4);
    kontorolClient.request(new UiConfListTemplatesAction())
      .then(
        response => {
	        asyncAssert(() => {
		        expect(response instanceof KontorolUiConfListResponse).toBeTruthy();
		        expect(Array.isArray(response.objects)).toBeTruthy();
		        expect(response.objects.length).toBeGreaterThan(0);
		        expect(response.objects[0] instanceof KontorolUiConf).toBeTruthy();
	        });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });
});
