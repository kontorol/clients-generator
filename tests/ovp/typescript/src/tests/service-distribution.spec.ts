import { KontorolClient } from "../kontorol-client-service";
import { DistributionProviderListAction } from "../api/types/DistributionProviderListAction";
import { KontorolDistributionProviderListResponse } from "../api/types/KontorolDistributionProviderListResponse";
import { KontorolDistributionProvider } from "../api/types/KontorolDistributionProvider";
import { DistributionProfileListAction } from "../api/types/DistributionProfileListAction";
import { KontorolDistributionProfileListResponse } from "../api/types/KontorolDistributionProfileListResponse";
import { KontorolDistributionProfile } from "../api/types/KontorolDistributionProfile";
import { EntryDistributionListAction } from "../api/types/EntryDistributionListAction";
import { KontorolEntryDistributionListResponse } from "../api/types/KontorolEntryDistributionListResponse";
import { KontorolEntryDistribution } from "../api/types/KontorolEntryDistribution";
import { getClient } from "./utils";
import { LoggerSettings, LogLevels } from "../api/kontorol-logger";
import { asyncAssert } from "./utils";

describe(`service "Distribution" tests`, () => {
	let kontorolClient: KontorolClient = null;

	beforeAll(async () => {
		LoggerSettings.logLevel = LogLevels.error; // suspend warnings

		return getClient()
			.then(client => {
				kontorolClient = client;
			}).catch(error => {
				// can do nothing since jasmine will ignore any exceptions thrown from before all
			});
	});

	test("distribution provider list", (done) => {
		expect.assertions(2);
		kontorolClient.request(new DistributionProviderListAction())
			.then(
				response => {
					asyncAssert(() => {
						expect(response instanceof KontorolDistributionProviderListResponse).toBeTruthy();
						expect(Array.isArray(response.objects)).toBeTruthy();

					});
					done();
				},
				(error) => {
					done.fail(error);
				});
	});

	test("distribution profile list", (done) => {
		expect.assertions(4);
		kontorolClient.request(new DistributionProfileListAction())
			.then(
				response => {
					asyncAssert(() => {
						expect(response instanceof KontorolDistributionProfileListResponse).toBeTruthy();
						expect(Array.isArray(response.objects)).toBeTruthy();
						expect(response.objects.length).toBeGreaterThan(0);
						expect(response.objects[0] instanceof KontorolDistributionProfile).toBeTruthy();
					});
					done();
				},
				() => {
					done.fail("should not reach this part");
				});
	});

	test("entry distribution list", (done) => {
		expect.assertions(2);
		kontorolClient.request(new EntryDistributionListAction())
			.then(
				response => {
					asyncAssert(() => {
						expect(response instanceof KontorolEntryDistributionListResponse).toBeTruthy();
						expect(Array.isArray(response.objects)).toBeTruthy();
					});
					done();
				},
				() => {
					done.fail("should not reach this part");
				});
	});
});
