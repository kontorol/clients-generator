import { KontorolClient } from "../kontorol-client-service";
import { WidgetListAction } from "../api/types/WidgetListAction";
import { KontorolWidgetListResponse } from "../api/types/KontorolWidgetListResponse";
import { getClient } from "./utils";
import { LoggerSettings, LogLevels } from "../api/kontorol-logger";
import { asyncAssert } from "./utils";

describe(`service "Widget" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return getClient()
      .then(client => {
        kontorolClient = client;
      }).catch(error => {
        // can do nothing since jasmine will ignore any exceptions thrown from before all
      });
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test("widgets list", (done) => {
	  expect.assertions(1);
    kontorolClient.request(new WidgetListAction())
      .then(
        response => {
	        asyncAssert(() => {
		        expect(response instanceof KontorolWidgetListResponse).toBeTruthy();
	        });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });
});
