// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================

import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.kontorol.client.KontorolApiException;
import com.kontorol.client.KontorolClient;
import com.kontorol.client.KontorolConfiguration;
import com.kontorol.client.KontorolMultiResponse;
import com.kontorol.client.enums.KontorolEntryStatus;
import com.kontorol.client.enums.KontorolMediaType;
import com.kontorol.client.enums.KontorolSessionType;
import com.kontorol.client.services.KontorolMediaService;
import com.kontorol.client.types.KontorolMediaEntry;
import com.kontorol.client.types.KontorolMediaListResponse;
import com.kontorol.client.types.KontorolPartner;
import com.kontorol.client.types.KontorolUploadToken;
import com.kontorol.client.types.KontorolUploadedFileTokenResource;

import com.kontorol.client.test.KontorolTestConfig;
import com.kontorol.client.test.TestUtils;

public class Kontorol {
	
	private static final int WAIT_BETWEEN_TESTS = 30000;
	protected static KontorolTestConfig testConfig;
	static public KontorolClient client;
	
	public static void main(String[] args) throws IOException {

		if(testConfig == null){
			testConfig = new KontorolTestConfig();
		}
		
		try {

			list();
			multiRequest();
			KontorolMediaEntry entry = addEmptyEntry();
			uploadMediaFileAndAttachToEmptyEntry(entry);
			testIfEntryIsReadyForPublish(entry);
			// cleanup the sample by deleting the entry:
			deleteEntry(entry);
			System.out.println("Sample code finished successfully.");
			
		} catch (KontorolApiException e) {
			System.out.println("Example failed.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Helper function to create the Kontorol client object once and then reuse a static instance.
	 * @return a singleton of <code>KontorolClient</code> used in this case
	 * @throws KontorolApiException if failed to generate session
	 */
	private static KontorolClient getKontorolClient() throws KontorolApiException
	{
		if (client != null) {
			return client;
		}
		
		// Set Constants
		int partnerId = testConfig.getPartnerId();
		String adminSecret = testConfig.getAdminSecret();
		String userId = testConfig.getUserId();
		
		// Generate configuration
		KontorolConfiguration config = new KontorolConfiguration();
		config.setEndpoint(testConfig.getServiceUrl());
		
		try {
			// Create the client and open session
			client = new KontorolClient(config);
			String ks = client.generateSession(adminSecret, userId, KontorolSessionType.ADMIN, partnerId);
			client.setSessionId(ks);
		} catch(Exception ex) {
			client = null;
			throw new KontorolApiException("Failed to generate session");
		}
		
		System.out.println("Generated KS locally: [" + client.getSessionId() + "]");
		return client;
	}
	
	/** 
	 * lists all media in the account.
	 */
	private static void list() throws KontorolApiException {

		KontorolMediaListResponse list = getKontorolClient().getMediaService().list();
		if (list.getTotalCount() > 0) {
			System.out.println("The account contains " + list.getTotalCount() + " entries.");
			for (KontorolMediaEntry entry : list.getObjects()) {
				System.out.println("\t \"" + entry.getName() + "\"");
			}
		} else {
			System.out.println("This account doesn't have any entries in it.");
		}
	}

	/**
	 * shows how to chain requests together to call a multi-request type where several requests are called in a single request.
	 */
	private static void multiRequest() throws KontorolApiException
 {
		KontorolClient client = getKontorolClient();
		client.startMultiRequest();
		client.getBaseEntryService().count();
		client.getPartnerService().getInfo();
		client.getPartnerService().getUsage(2010);
		KontorolMultiResponse multi = client.doMultiRequest();
		KontorolPartner partner = (KontorolPartner) multi.get(1);
		System.out.println("Got Admin User email: " + partner.getAdminEmail());

	}
	
	/** 
	 * creates an empty media entry and assigns basic metadata to it.
	 * @return the generated <code>KontorolMediaEntry</code>
	 * @throws KontorolApiException
	 */
	private static KontorolMediaEntry addEmptyEntry() throws KontorolApiException {
		System.out.println("Creating an empty Kontorol Entry (without actual media binary attached)...");
		KontorolMediaEntry entry = new KontorolMediaEntry();
		entry.setName("An Empty Kontorol Entry Test");
		entry.setMediaType(KontorolMediaType.VIDEO);
		KontorolMediaEntry newEntry = getKontorolClient().getMediaService().add(entry);
		System.out.println("The id of our new Video Entry is: " + newEntry.getId());
		return newEntry;
	}
	
	/**
	 *  uploads a video file to Kontorol and assigns it to a given Media Entry object
	 */
	private static void uploadMediaFileAndAttachToEmptyEntry(KontorolMediaEntry entry) throws KontorolApiException
	{
			KontorolClient client = getKontorolClient();
			System.out.println("Uploading a video file...");
			
			// upload upload token
			KontorolUploadToken upToken = client.getUploadTokenService().add();
			KontorolUploadedFileTokenResource fileTokenResource = new KontorolUploadedFileTokenResource();
			
			// Connect to media entry and update name
			fileTokenResource.setToken(upToken.getId());
			entry = client.getMediaService().addContent(entry.getId(), fileTokenResource);
			
			// Upload actual data
			try
			{
				InputStream fileData = TestUtils.getTestVideo();
				int fileSize = fileData.available();

				client.getUploadTokenService().upload(upToken.getId(), fileData, testConfig.getUploadVideo(), fileSize);
				
				System.out.println("Uploaded a new Video file to entry: " + entry.getId());
			}
			catch (FileNotFoundException e)
			{
				System.out.println("Failed to open test video file");
			}
			catch (IOException e)
			{
				System.out.println("Failed to read test video file");
			}
	}
	
	/** 
	 * periodically calls the Kontorol API to check that a given video entry has finished transcoding and is ready for playback.
	 * @param entry The <code>KontorolMediaEntry</code> we want to test
	 */
	private static void testIfEntryIsReadyForPublish(KontorolMediaEntry entry)
			throws KontorolApiException {

		System.out.println("Testing if Media Entry has finished processing and ready to be published...");
		KontorolMediaService mediaService = getKontorolClient().getMediaService();
		while (true) {
			KontorolMediaEntry retrievedEntry = mediaService.get(entry.getId());
			if (retrievedEntry.getStatus() == KontorolEntryStatus.READY) {
				break;
			}
			System.out.println("Media not ready yet. Waiting 30 seconds.");
			try {
				Thread.sleep(WAIT_BETWEEN_TESTS);
			} catch (InterruptedException ie) {
			}
		}
		System.out.println("Entry id: " + entry.getId() + " is now ready to be published and played.");
	}

	/** 
	 * deletes a given entry
	 * @param entry the <code>KontorolMediaEntry</code> we want to delete
	 * @throws KontorolApiException
	 */
	private static void deleteEntry(KontorolMediaEntry entry)
			throws KontorolApiException {
		System.out.println("Deleting entry id: " + entry.getId());
		getKontorolClient().getMediaService().delete(entry.getId());
	}
}
