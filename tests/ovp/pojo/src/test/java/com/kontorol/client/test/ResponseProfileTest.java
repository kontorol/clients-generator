// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================
package com.kontorol.client.test;

import java.util.ArrayList;

import com.kontorol.client.enums.KontorolMediaType;
import com.kontorol.client.enums.KontorolMetadataObjectType;
import com.kontorol.client.types.KontorolCategory;
import com.kontorol.client.types.KontorolCategoryEntry;
import com.kontorol.client.types.KontorolCategoryEntryFilter;
import com.kontorol.client.types.KontorolCategoryEntryListResponse;
import com.kontorol.client.types.KontorolDetachedResponseProfile;
import com.kontorol.client.types.KontorolMediaEntry;
import com.kontorol.client.types.KontorolMetadata;
import com.kontorol.client.types.KontorolMetadataFilter;
import com.kontorol.client.types.KontorolMetadataListResponse;
import com.kontorol.client.types.KontorolMetadataProfile;
import com.kontorol.client.types.KontorolResponseProfile;
import com.kontorol.client.types.KontorolResponseProfileHolder;
import com.kontorol.client.types.KontorolResponseProfileMapping;


public class ResponseProfileTest extends BaseTest{

	public void testEntryCategoriesAndMetadata() throws Exception {
		KontorolMediaEntry entry = null;
		KontorolCategory category = null;
		KontorolMetadataProfile categoryMetadataProfile = null;
		KontorolResponseProfile responseProfile = null;
		
		try{
			String xsd = "<xsd:schema xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n";
			xsd += "	<xsd:element name=\"metadata\">\n";
			xsd += "		<xsd:complexType>\n";
			xsd += "			<xsd:sequence>\n";
			xsd += "				<xsd:element name=\"Choice\" minOccurs=\"0\" maxOccurs=\"1\">\n";
			xsd += "					<xsd:annotation>\n";
			xsd += "						<xsd:documentation></xsd:documentation>\n";
			xsd += "						<xsd:appinfo>\n";
			xsd += "							<label>Example choice</label>\n";
			xsd += "							<key>choice</key>\n";
			xsd += "							<searchable>true</searchable>\n";
			xsd += "							<description>Example choice</description>\n";
			xsd += "						</xsd:appinfo>\n";
			xsd += "					</xsd:annotation>\n";
			xsd += "					<xsd:simpleType>\n";
			xsd += "						<xsd:restriction base=\"listType\">\n";
			xsd += "							<xsd:enumeration value=\"on\" />\n";
			xsd += "							<xsd:enumeration value=\"off\" />\n";
			xsd += "						</xsd:restriction>\n";
			xsd += "					</xsd:simpleType>\n";
			xsd += "				</xsd:element>\n";
			xsd += "				<xsd:element name=\"FreeText\" minOccurs=\"0\" maxOccurs=\"1\" type=\"textType\">\n";
			xsd += "					<xsd:annotation>\n";
			xsd += "						<xsd:documentation></xsd:documentation>\n";
			xsd += "						<xsd:appinfo>\n";
			xsd += "							<label>Free text</label>\n";
			xsd += "							<key>freeText</key>\n";
			xsd += "							<searchable>true</searchable>\n";
			xsd += "							<description>Free text</description>\n";
			xsd += "						</xsd:appinfo>\n";
			xsd += "					</xsd:annotation>\n";
			xsd += "				</xsd:element>\n";
			xsd += "			</xsd:sequence>\n";
			xsd += "		</xsd:complexType>\n";
			xsd += "	</xsd:element>\n";
			xsd += "	<xsd:complexType name=\"textType\">\n";
			xsd += "		<xsd:simpleContent>\n";
			xsd += "			<xsd:extension base=\"xsd:string\" />\n";
			xsd += "		</xsd:simpleContent>\n";
			xsd += "	</xsd:complexType>\n";
			xsd += "	<xsd:complexType name=\"objectType\">\n";
			xsd += "		<xsd:simpleContent>\n";
			xsd += "			<xsd:extension base=\"xsd:string\" />\n";
			xsd += "		</xsd:simpleContent>\n";
			xsd += "	</xsd:complexType>\n";
			xsd += "	<xsd:simpleType name=\"listType\">\n";
			xsd += "		<xsd:restriction base=\"xsd:string\" />\n";
			xsd += "	</xsd:simpleType>\n";
			xsd += "</xsd:schema>";
			
			String xml = "<metadata>\n";
			xml += "	<Choice>on</Choice>\n";
			xml += "	<FreeText>example text </FreeText>\n";
			xml += "</metadata>";
						
			entry = createEntry();
			category = createCategory();
			categoryMetadataProfile = createMetadataProfile(KontorolMetadataObjectType.CATEGORY, xsd);

			KontorolMetadataFilter metadataFilter = new KontorolMetadataFilter();
			metadataFilter.setMetadataObjectTypeEqual(KontorolMetadataObjectType.CATEGORY);
			metadataFilter.setMetadataProfileIdEqual(categoryMetadataProfile.getId());

			KontorolResponseProfileMapping metadataMapping = new KontorolResponseProfileMapping();
			metadataMapping.setFilterProperty("objectIdEqual");
			metadataMapping.setParentProperty("categoryId");
			
			ArrayList<KontorolResponseProfileMapping> metadataMappings = new ArrayList<KontorolResponseProfileMapping>();
			metadataMappings.add(metadataMapping);

			KontorolDetachedResponseProfile metadataResponseProfile = new KontorolDetachedResponseProfile();
			metadataResponseProfile.setName("metadata");
			metadataResponseProfile.setFilter(metadataFilter);
			metadataResponseProfile.setMappings(metadataMappings);
			
			ArrayList<KontorolDetachedResponseProfile> categoryEntryRelatedProfiles = new ArrayList<KontorolDetachedResponseProfile>();
			categoryEntryRelatedProfiles.add(metadataResponseProfile);

			KontorolCategoryEntryFilter categoryEntryFilter = new KontorolCategoryEntryFilter();
			
			KontorolResponseProfileMapping categoryEntryMapping = new KontorolResponseProfileMapping();
			categoryEntryMapping.setFilterProperty("entryIdEqual");
			categoryEntryMapping.setParentProperty("id");
			
			ArrayList<KontorolResponseProfileMapping> categoryEntryMappings = new ArrayList<KontorolResponseProfileMapping>();
			categoryEntryMappings.add(categoryEntryMapping);
			
			KontorolDetachedResponseProfile categoryEntryResponseProfile = new KontorolDetachedResponseProfile();
			categoryEntryResponseProfile.setName("categoryEntry");
			categoryEntryResponseProfile.setRelatedProfiles(categoryEntryRelatedProfiles);
			categoryEntryResponseProfile.setFilter(categoryEntryFilter);
			categoryEntryResponseProfile.setMappings(categoryEntryMappings);
			
			ArrayList<KontorolDetachedResponseProfile> entryRelatedProfiles = new ArrayList<KontorolDetachedResponseProfile>();
			entryRelatedProfiles.add(categoryEntryResponseProfile);
			
			responseProfile = new KontorolResponseProfile();
			responseProfile.setName("rp" + System.currentTimeMillis());
			responseProfile.setSystemName(responseProfile.getName());
			responseProfile.setRelatedProfiles(entryRelatedProfiles);
			
			responseProfile = client.getResponseProfileService().add(responseProfile);
			assertNotNull(responseProfile.getId());
			assertNotNull(responseProfile.getRelatedProfiles());
			assertEquals(1, responseProfile.getRelatedProfiles().size());
			
			KontorolCategoryEntry categoryEntry = addEntryToCategory(entry.getId(), category.getId());
			KontorolMetadata categoryMetadata = createMetadata(KontorolMetadataObjectType.CATEGORY, Integer.toString(category.getId()), categoryMetadataProfile.getId(), xml);
			
			KontorolResponseProfileHolder responseProfileHolder = new KontorolResponseProfileHolder();
			responseProfileHolder.setId(responseProfile.getId());
	
			startAdminSession();
			client.setResponseProfile(responseProfileHolder);
			KontorolMediaEntry getEntry = client.getMediaService().get(entry.getId());
			assertEquals(getEntry.getId(), entry.getId());
			
			assertNotNull(getEntry.getRelatedObjects());
			assertTrue(getEntry.getRelatedObjects().containsKey(categoryEntryResponseProfile.getName()));
			KontorolCategoryEntryListResponse categoryEntryList = (KontorolCategoryEntryListResponse) getEntry.getRelatedObjects().get(categoryEntryResponseProfile.getName());
			assertEquals(1, categoryEntryList.getTotalCount());
			KontorolCategoryEntry getCategoryEntry = categoryEntryList.getObjects().get(0);
			assertEquals(getCategoryEntry.getCreatedAt(), categoryEntry.getCreatedAt());

			assertNotNull(getCategoryEntry.getRelatedObjects());
			assertTrue(getCategoryEntry.getRelatedObjects().containsKey(metadataResponseProfile.getName()));
			KontorolMetadataListResponse metadataList = (KontorolMetadataListResponse) getCategoryEntry.getRelatedObjects().get(metadataResponseProfile.getName());
			assertEquals(1, metadataList.getTotalCount());
			KontorolMetadata getMetadata = metadataList.getObjects().get(0);
			assertEquals(categoryMetadata.getId(), getMetadata.getId());
			assertEquals(xml, getMetadata.getXml());
		}
		finally{
			if(responseProfile != null && responseProfile.getId() > 0)
				deleteResponseProfile(responseProfile.getId());

			if(entry != null && entry.getId() != null)
				deleteEntry(entry.getId());

			if(category != null && category.getId() > 0)
				deleteCategory(category.getId());

			if(categoryMetadataProfile != null && categoryMetadataProfile.getId() > 0)
				deleteMetadataProfile(categoryMetadataProfile.getId());
		}
	}

	protected KontorolMetadata createMetadata(KontorolMetadataObjectType objectType, String objectId, int metadataProfileId, String xmlData) throws Exception {
		startAdminSession();

		KontorolMetadata metadata = client.getMetadataService().add(metadataProfileId, objectType, objectId, xmlData);
		assertNotNull(metadata.getId());
		
		return metadata;
	}

	protected KontorolMetadataProfile createMetadataProfile(KontorolMetadataObjectType objectType, String xsdData) throws Exception {
		startAdminSession();

		KontorolMetadataProfile metadataProfile = new KontorolMetadataProfile();
		metadataProfile.setMetadataObjectType(objectType);
		metadataProfile.setName("mp" + System.currentTimeMillis());
		
		metadataProfile = client.getMetadataProfileService().add(metadataProfile, xsdData);
		assertNotNull(metadataProfile.getId());
		
		return metadataProfile;
	}

	protected KontorolCategoryEntry addEntryToCategory(String entryId, int categoryId) throws Exception {
		startAdminSession();

		KontorolCategoryEntry categoryEntry = new KontorolCategoryEntry();
		categoryEntry.setEntryId(entryId);
		categoryEntry.setCategoryId(categoryId);
		
		categoryEntry = client.getCategoryEntryService().add(categoryEntry);
		assertNotNull(categoryEntry.getCreatedAt());
		
		return categoryEntry;
	}

	protected KontorolMediaEntry createEntry() throws Exception {
		startAdminSession();

		KontorolMediaEntry entry = new KontorolMediaEntry();
		entry.setMediaType(KontorolMediaType.VIDEO);
		
		entry = client.getMediaService().add(entry);
		assertNotNull(entry.getId());
		
		return entry;
	}

	protected KontorolCategory createCategory() throws Exception {
		startAdminSession();

		KontorolCategory category = new KontorolCategory();
		category.setName("c" + System.currentTimeMillis());
		
		category = client.getCategoryService().add(category);
		assertNotNull(category.getId());
		
		return category;
	}

	protected void deleteCategory(int id) throws Exception {
		startAdminSession();
		client.getCategoryService().delete(id);
	}

	protected void deleteEntry(String id) throws Exception {
		startAdminSession();
		client.getBaseEntryService().delete(id);
	}

	protected void deleteResponseProfile(int id) throws Exception {
		startAdminSession();
		client.getResponseProfileService().delete(id);
	}

	protected void deleteMetadataProfile(int id) throws Exception {
		startAdminSession();
		client.getMetadataProfileService().delete(id);
	}
	
}
