
var config = new KontorolConfiguration();
config.serviceUrl = serviceUrl;
config.setLogger(new IKontorolLogger());

var client = new KontorolClient(config);

describe("Start session", function() {
    describe("User KS", function() {
    	var userId = null;
    	var type = 0; // KontorolSessionType.USER
    	var expiry = null;
    	var privileges = null;

    	it('not null', function(done) {
    		KontorolSessionService.start(secret, userId, type, partnerId, expiry, privileges)
        	.completion(function(success, ks) {
        		expect(success).toBe(true);
        		expect(ks).not.toBe(null);
        		client.setKs(ks);
        		done();
        	})
        	.execute(client);
        });
    });
});


describe("media", function() {

    describe("multi-request", function() {
    	var entry = {
    		mediaType: 1, // KontorolMediaType.VIDEO
    		name: 'test'
    	};

    	var uploadToken = {
    	};

    	var mediaResource = {
    		objectType: 'KontorolUploadedFileTokenResource',
			token: '{2:result:id}'
    	};

		var filename = './DemoVideo.mp4';
		
    	jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
    	it('create entry', function(done) {
    		KontorolMediaService.add(entry)
    		.add(KontorolUploadTokenService.add(uploadToken))
    		.add(KontorolMediaService.addContent('{1:result:id}', mediaResource))
//    		Karma doesn't support creating <input type=file>
//    		.add(KontorolUploadTokenService.upload('{2:result:id}', filename))
    		.completion(function(success, results) {
        		expect(success).toBe(true);
        		
    			entry = results[0];
        		expect(entry).not.toBe(null);
        		expect(entry.id).not.toBe(null);
        		expect(entry.status.toString()).toBe('7'); // KontorolEntryStatus.NO_CONTENT

    			uploadToken = results[1];
        		expect(uploadToken).not.toBe(null);
        		expect(uploadToken.id).not.toBe(null);
        		expect(uploadToken.status).toBe(0); // KontorolUploadTokenStatus.PENDING

    			entry = results[2];
        		expect(entry.status.toString()).toBe('0'); // KontorolEntryStatus.IMPORT

//        		Karma doesn't support creating <input type=file>
//    			uploadToken = results[3];
//        		expect(uploadToken).not.toBe(null);
//        		expect(uploadToken.id).not.toBe(null);
//        		expect(uploadToken.fileSize).toBeGreaterThan(0);
//        		expect(uploadToken.status).toBe(3); // KontorolUploadTokenStatus.CLOSED
        		
        		done();
    		})
    		.execute(client);
    	});
    });
});
