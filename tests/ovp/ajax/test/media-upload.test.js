
var config = new KontorolConfiguration();
config.serviceUrl = serviceUrl;
config.setLogger(new IKontorolLogger());

var client = new KontorolClient(config);

describe("Start session", function() {
    describe("User KS", function() {
    	var userId = null;
    	var type = 0; // KontorolSessionType.USER
    	var expiry = null;
    	var privileges = null;

    	it('not null', function(done) {
    		KontorolSessionService.start(secret, userId, type, partnerId, expiry, privileges)
        	.completion(function(success, ks) {
        		expect(success).toBe(true);
        		expect(ks).not.toBe(null);
        		client.setKs(ks);
        		done();
        	})
        	.execute(client);
        });
    });
});


describe("media", function() {
    describe("upload", function() {

    	var entry = {
    		mediaType: 1, // KontorolMediaType.VIDEO
    		name: 'test'
    	};

    	var uploadToken = {};

    	var createdEntry;
    	var createdUploadToken;
    	
    	it('create entry', function(done) {
    		KontorolMediaService.add(entry)
    		.completion(function(success, entry) {
        		expect(success).toBe(true);
        		expect(entry).not.toBe(null);
        		expect(entry.id).not.toBe(null);
        		expect(entry.status.toString()).toBe('7'); // KontorolEntryStatus.NO_CONTENT

        		createdEntry = entry;
        		done();
    		})
    		.execute(client)
        });
        
    	it('create upload-token', function(done) {
    		KontorolUploadTokenService.add(uploadToken)
    		.completion(function(success, uploadToken) {
        		expect(success).toBe(true);
        		expect(uploadToken).not.toBe(null);
        		expect(uploadToken.id).not.toBe(null);
        		expect(uploadToken.status).toBe(0); // KontorolUploadTokenStatus.PENDING

        		createdUploadToken = uploadToken;
        		done();
    		})
    		.execute(client);
        });
        
    	it('add content', function(done) {
    		var mediaResource = {
    			objectType: 'KontorolUploadedFileTokenResource',
    			token: createdUploadToken.id
        	};
    		
    		KontorolMediaService.addContent(createdEntry.id, mediaResource)
    		.completion(function(success, entry) {
        		expect(success).toBe(true);
        		expect(entry.status.toString()).toBe('0'); // KontorolEntryStatus.IMPORT

        		done();
    		})
    		.execute(client);
    	});
        
//    	Karma doesn't support creating <input type=file> 
//    	it('upload file', function(done) {
//    		var filename = './DemoVideo.mp4';
//    		KontorolUploadTokenService.upload(createdUploadToken.id, filename)
//    		.completion(function(success, uploadToken) {
//        		expect(success).toBe(true);
//        		expect(uploadToken).not.toBe(null);
//        		expect(uploadToken.id).not.toBe(null);
//        		expect(uploadToken.fileSize).toBeGreaterThan(0);
//        		expect(uploadToken.status).toBe(3); // KontorolUploadTokenStatus.CLOSED
//
//        		createdUploadToken = uploadToken;
//        		done();
//    		})
//    		.execute(client);
//        });
    });
});
