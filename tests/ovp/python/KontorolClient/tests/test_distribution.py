from __future__ import absolute_import

import unittest

from .utils import KontorolBaseTest

from KontorolClient.Plugins.ContentDistribution import (
    KontorolDistributionProfile,
    KontorolDistributionProfileListResponse,
    KontorolDistributionProvider,
    KontorolDistributionProviderListResponse,
    KontorolEntryDistribution,
    KontorolEntryDistributionListResponse,
    )


class DistributionProviderTests(KontorolBaseTest):

    def test_list(self):
        resp = self.client.contentDistribution.distributionProvider.list()
        self.assertIsInstance(resp, KontorolDistributionProviderListResponse)

        objs = resp.objects
        self.assertIsInstance(objs, list)

        [self.assertIsInstance(o, KontorolDistributionProvider) for o in objs]


class DistributionProfileTests(KontorolBaseTest):

    def test_list(self):
        resp = self.client.contentDistribution.distributionProfile.list()
        self.assertIsInstance(resp, KontorolDistributionProfileListResponse)

        objs = resp.objects
        self.assertIsInstance(objs, list)

        [self.assertIsInstance(o, KontorolDistributionProfile) for o in objs]


class EntryDistributionTests(KontorolBaseTest):

    def test_list(self):
        resp = self.client.contentDistribution.entryDistribution.list()
        self.assertIsInstance(resp, KontorolEntryDistributionListResponse)

        objs = resp.objects
        self.assertIsInstance(objs, list)

        [self.assertIsInstance(o, KontorolEntryDistribution) for o in objs]


def test_suite():
    return unittest.TestSuite((
        unittest.makeSuite(DistributionProviderTests),
        unittest.makeSuite(DistributionProfileTests),
        unittest.makeSuite(EntryDistributionTests),
        ))


if __name__ == "__main__":
    suite = test_suite()
    unittest.TextTestRunner(verbosity=2).run(suite)
