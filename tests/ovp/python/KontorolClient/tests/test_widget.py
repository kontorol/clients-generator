from __future__ import absolute_import

import unittest

from .utils import KontorolBaseTest

from KontorolClient.Plugins.Core import KontorolWidgetListResponse


class WidgetTests(KontorolBaseTest):

    def test_list_widgets(self):
        widgets = self.client.widget.list()
        self.assertIsInstance(widgets, KontorolWidgetListResponse)


def test_suite():
    return unittest.TestSuite((
        unittest.makeSuite(WidgetTests),
        ))


if __name__ == "__main__":
    suite = test_suite()
    unittest.TextTestRunner(verbosity=2).run(suite)
