import os, sys, inspect
import unittest

from six.moves import configparser

from KontorolClient import KontorolClient, KontorolConfiguration
from KontorolClient.Base import KontorolObjectFactory, KontorolEnumsFactory
from KontorolClient.Base import IKontorolLogger

from KontorolClient.Plugins.Core import KontorolSessionType

generateSessionFunction = KontorolClient.generateSessionV2
# generateSessionV2() needs the Crypto module, if we don't have it, we fallback to generateSession()
try:
    from Crypto import Random
    from Crypto.Cipher import AES
except ImportError:
    generateSessionFunction = KontorolClient.generateSession

dir = os.path.dirname(__file__)
filename = os.path.join(dir, 'config.ini')

config = configparser.ConfigParser()
config.read(filename)
PARTNER_ID = config.getint("Test", "partnerId")
SERVICE_URL = config.get("Test", "serviceUrl")
ADMIN_SECRET = config.get("Test", "adminSecret")
USER_NAME = config.get("Test", "userName")

import logging
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s %(levelname)s %(message)s',
                    stream = sys.stdout)

class KontorolLogger(IKontorolLogger):
    def log(self, msg):
        logging.info(msg)

def GetConfig():
    config = KontorolConfiguration()
    config.requestTimeout = 500
    config.serviceUrl = SERVICE_URL
    config.setLogger(KontorolLogger())
    return config

def getTestFile(filename, mode='rb'):
    testFileDir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    return open(testFileDir+'/'+filename, mode)
    
    

class KontorolBaseTest(unittest.TestCase):
    """Base class for all Kontorol Tests"""
    #TODO  create a client factory as to avoid thrashing kontorol with logins...
    
    def setUp(self):
        #(client session is enough when we do operations in a users scope)
        self.config = GetConfig()
        self.client = KontorolClient(self.config)
        self.ks = generateSessionFunction(ADMIN_SECRET, USER_NAME, 
                                             KontorolSessionType.ADMIN, PARTNER_ID,
                                             86400, "disableentitlement")
        self.client.setKs(self.ks)            
            
            
    def tearDown(self):
        
        #do cleanup first, probably relies on self.client
        self.doCleanups()
        
        del(self.ks)
        del(self.client)
        del(self.config)
        
