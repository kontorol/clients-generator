from __future__ import absolute_import

import uuid
import unittest

from .utils import KontorolBaseTest

from KontorolClient.Plugins.Core import (
    KontorolBaseEntryListResponse,
    KontorolDetachedResponseProfile,
    KontorolEntryStatus,
    KontorolFilterPager,
    KontorolMediaEntry,
    KontorolMediaEntryFilter,
    KontorolMediaType,
    KontorolResponseProfile,
    KontorolResponseProfileHolder,
    KontorolResponseProfileMapping,
    KontorolResponseProfileType,
)
from KontorolClient.Plugins.Metadata import (
    KontorolMetadata,
    KontorolMetadataFilter,
    KontorolMetadataListResponse,
    KontorolMetadataObjectType,
    KontorolMetadataProfile,
)


class ResponseProfileTests(KontorolBaseTest):

    def setUp(self):
        KontorolBaseTest.setUp(self)
        self.uniqueTag = self.uniqid('tag_')

    def uniqid(self, prefix):
        return prefix + uuid.uuid1().hex

    def createEntry(self):
        entry = KontorolMediaEntry()
        entry.mediaType = KontorolMediaType.VIDEO
        entry.name = self.uniqid('test_')
        entry.description = self.uniqid('test ')
        entry.tags = self.uniqueTag

        entry = self.client.media.add(entry)

        return entry

    def createMetadata(self, metadataProfileId, objectType, objectId, xmlData):
        metadata = KontorolMetadata()
        metadata.metadataObjectType = objectType
        metadata.objectId = objectId

        metadata = self.client.metadata.metadata.add(
            metadataProfileId, objectType, objectId, xmlData)

        return metadata

    def createMetadataProfile(self, objectType, xsdData):
        metadataProfile = KontorolMetadataProfile()
        metadataProfile.metadataObjectType = objectType
        metadataProfile.name = self.uniqid('test_')
        metadataProfile.systemName = self.uniqid('test_')

        metadataProfile = self.client.metadata.metadataProfile.add(
            metadataProfile, xsdData)

        return metadataProfile

    def createEntriesWithMetadataObjects(self, entriesCount,
                                         metadataProfileCount=2):
        entries = []
        metadataProfiles = {}
        xsd = """\
<xsd:schema xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
    <xsd:element name=\"metadata\">
        <xsd:complexType>
            <xsd:sequence>
                <xsd:element name=\"Choice{index}\" minOccurs=\"0\"
                    maxOccurs=\"1\">
                    <xsd:annotation>
                        <xsd:documentation></xsd:documentation>
                        <xsd:appinfo>
                            <label>Example choice {index}</label>
                            <key>choice{index}</key>
                            <searchable>true</searchable>
                            <description>Example choice {index}</description>
                        </xsd:appinfo>
                    </xsd:annotation>
                    <xsd:simpleType>
                        <xsd:restriction base=\"listType\">
                            <xsd:enumeration value=\"on\" />
                            <xsd:enumeration value=\"off\" />
                        </xsd:restriction>
                    </xsd:simpleType>
                </xsd:element>
                <xsd:element name=\"FreeText{index}\" minOccurs=\"0\"
                    maxOccurs=\"1\" type=\"textType\">
                    <xsd:annotation>
                        <xsd:documentation></xsd:documentation>
                        <xsd:appinfo>
                            <label>Free text {index}</label>
                            <key>freeText{index}</key>
                            <searchable>true</searchable>
                            <description>Free text {index}</description>
                        </xsd:appinfo>
                    </xsd:annotation>
                </xsd:element>
            </xsd:sequence>
        </xsd:complexType>
    </xsd:element>
    <xsd:complexType name=\"textType\">
        <xsd:simpleContent>
            <xsd:extension base=\"xsd:string\" />
        </xsd:simpleContent>
    </xsd:complexType>
    <xsd:complexType name=\"objectType\">
        <xsd:simpleContent>
            <xsd:extension base=\"xsd:string\" />
        </xsd:simpleContent>
    </xsd:complexType>
    <xsd:simpleType name=\"listType\">
        <xsd:restriction base=\"xsd:string\" />
    </xsd:simpleType>
</xsd:schema>"""
        for i in range(1, metadataProfileCount + 1):
            metadataProfiles[i] = self.createMetadataProfile(
                KontorolMetadataObjectType.ENTRY, xsd.format(index=i))

        xml = """\
<metadata>
    <Choice{index}>on</Choice{index}>
    <FreeText{index}>example text {index}</FreeText{index}>
</metadata>
"""
        for i in range(0, entriesCount):
            entry = self.createEntry()
            entries.append(entry)

            for j in range(1, metadataProfileCount + 1):
                self.createMetadata(
                    metadataProfiles[j].id,
                    KontorolMetadataObjectType.ENTRY, entry.id,
                    xml.format(index=j))

        return [entries, metadataProfiles]

    def test_list(self):
        entriesTotalCount = 4
        entriesPageSize = 3
        metadataPageSize = 2

        entries, metadataProfiles = self.createEntriesWithMetadataObjects(
            entriesTotalCount, metadataPageSize)

        entriesFilter = KontorolMediaEntryFilter()
        entriesFilter.tagsLike = self.uniqueTag
        entriesFilter.statusIn = "{},{}".format(
            KontorolEntryStatus.PENDING, KontorolEntryStatus.NO_CONTENT)

        entriesPager = KontorolFilterPager()
        entriesPager.pageSize = entriesPageSize

        metadataFilter = KontorolMetadataFilter()
        metadataFilter.metadataObjectTypeEqual = (
            KontorolMetadataObjectType.ENTRY)

        metadataMapping = KontorolResponseProfileMapping()
        metadataMapping.filterProperty = 'objectIdEqual'
        metadataMapping.parentProperty = 'id'

        metadataPager = KontorolFilterPager()
        metadataPager.pageSize = metadataPageSize

        metadataResponseProfile = KontorolDetachedResponseProfile()
        metadataResponseProfile.name = self.uniqid('test_')
        metadataResponseProfile.type = (
            KontorolResponseProfileType.INCLUDE_FIELDS)
        metadataResponseProfile.fields = 'id,objectId,createdAt, xml'
        metadataResponseProfile.filter = metadataFilter
        metadataResponseProfile.pager = metadataPager
        metadataResponseProfile.mappings = [metadataMapping]

        responseProfile = KontorolResponseProfile()
        responseProfile.name = self.uniqid('test_')
        responseProfile.systemName = self.uniqid('test_')
        responseProfile.type = KontorolResponseProfileType.INCLUDE_FIELDS
        responseProfile.fields = 'id,name,createdAt'
        responseProfile.relatedProfiles = [metadataResponseProfile]

        responseProfile = self.client.responseProfile.add(responseProfile)

        nestedResponseProfile = KontorolResponseProfileHolder()
        nestedResponseProfile.id = responseProfile.id

        self.client.setResponseProfile(nestedResponseProfile)
        list_ = self.client.baseEntry.list(entriesFilter, entriesPager)

        self.assertIsInstance(list_, KontorolBaseEntryListResponse)
        self.assertEqual(entriesTotalCount, list_.totalCount)
        self.assertEqual(entriesPageSize, len(list_.objects))
        [self.assertIsInstance(entry, KontorolMediaEntry)
         for entry in list_.objects]

        for entry in list_.objects:
            self.assertNotEqual(entry.relatedObjects, NotImplemented)
            self.assertIn(metadataResponseProfile.name, entry.relatedObjects)
            metadataList = entry.relatedObjects[metadataResponseProfile.name]
            self.assertIsInstance(metadataList, KontorolMetadataListResponse)
            self.assertEqual(len(metadataProfiles), len(metadataList.objects))
            for metadata in metadataList.objects:
                self.assertIsInstance(metadata, KontorolMetadata)
                self.assertEqual(entry.id, metadata.objectId)


def test_suite():
    return unittest.TestSuite((
        unittest.makeSuite(ResponseProfileTests)
        ))


if __name__ == "__main__":
    suite = test_suite()
    unittest.TextTestRunner(verbosity=2).run(suite)
