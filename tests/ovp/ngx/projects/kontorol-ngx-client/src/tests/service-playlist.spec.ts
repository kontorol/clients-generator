import {PlaylistListAction} from "../lib/api/types/PlaylistListAction";
import {KontorolPlaylistListResponse} from "../lib/api/types/KontorolPlaylistListResponse";
import {KontorolPlaylist} from "../lib/api/types/KontorolPlaylist";
import {KontorolPlaylistType} from "../lib/api/types/KontorolPlaylistType";
import {PlaylistAddAction} from "../lib/api/types/PlaylistAddAction";
import {PlaylistDeleteAction} from "../lib/api/types/PlaylistDeleteAction";
import {PlaylistUpdateAction} from "../lib/api/types/PlaylistUpdateAction";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolClient} from "../lib/kontorol-client.service";
import { switchMap } from 'rxjs/operators';


describe(`service "Playlist" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return new Promise((resolve => {
      getClient()
        .subscribe(client => {
          kontorolClient = client;
          resolve(client);
        });
    }));
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test(`invoke "list" action`, (done) => {
    expect.assertions(4);
    kontorolClient.request(new PlaylistListAction()).subscribe(
      (response) => {
        asyncAssert(() => {
          expect(response instanceof KontorolPlaylistListResponse).toBeTruthy();
          expect(response.objects).toBeDefined();
          expect(response.objects instanceof Array).toBeTruthy();
          expect(response.objects[0] instanceof KontorolPlaylist).toBeTruthy();
        });

        done();
      },
      () => {
        done.fail(`failed to perform request`);
      }
    );
  });

  test(`invoke "createRemote:staticList" action`, (done) => {
    const playlist = new KontorolPlaylist({
      name: "tstest.PlaylistTests.test_createRemote",
      playlistType: KontorolPlaylistType.staticList
    });
    expect.assertions(2);
    kontorolClient.request(new PlaylistAddAction({playlist}))
      .subscribe(
        (response) => {
          kontorolClient.request(new PlaylistDeleteAction({id: response.id})).subscribe(
            () => {
              asyncAssert(() => {
                expect(response instanceof KontorolPlaylist).toBeTruthy();
                expect(typeof response.id).toBe("string");
              });
              done();
            }
          );
        },
        (error) => {
          done.fail(error);
        }
      );
  });

  test(`invoke "update" action`, (done) => {
    const playlist = new KontorolPlaylist({
      name: "tstest.PlaylistTests.test_createRemote",
      referenceId: "tstest.PlaylistTests.test_update",
      playlistType: KontorolPlaylistType.staticList
    });
    expect.assertions(1);
    kontorolClient.request(new PlaylistAddAction({playlist}))
      .pipe(
      switchMap(({id}) => {
          playlist.name = "Changed!";
          return kontorolClient.request(new PlaylistUpdateAction({id, playlist}));
        }
      ),
      switchMap(({id, name}) => {
        asyncAssert(() => {
          expect(name).toBe("Changed!");
        });
        return kontorolClient.request(new PlaylistDeleteAction({id}));
      }))
      .subscribe(() => {
            done();
        },
        error => {
          done.fail(error);
        });
  });

  test(`invoke "createRemote:dynamicList" action`, (done) => {
    const playlist = new KontorolPlaylist({
      name: "tstest.PlaylistTests.test_createRemote",
      playlistType: KontorolPlaylistType.dynamic,
      totalResults: 0
    });
    expect.assertions(2);
    kontorolClient.request(new PlaylistAddAction({playlist}))
      .subscribe(
        (response) => {
          kontorolClient.request(new PlaylistDeleteAction({id: response.id}))
            .subscribe(() => {
              asyncAssert(() => {
                expect(response instanceof KontorolPlaylist).toBeTruthy();
                expect(typeof response.id).toBe("string");
              });
              done();
            });
        },
        (error) => {
          done.fail(error);
        }
      );
  });
});
