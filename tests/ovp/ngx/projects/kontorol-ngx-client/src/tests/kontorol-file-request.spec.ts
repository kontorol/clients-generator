import {ThumbAssetServeAction} from "../lib/api/types/ThumbAssetServeAction";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolClient} from "../lib/kontorol-client.service";
import { environment } from '../lib/environment';


describe("Kontorol File request", () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return new Promise((resolve => {
      getClient()
        .subscribe(client => {
          kontorolClient = client;
          resolve(client);
        });
    }));
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test("thumbasset service > serve action", (done) => {

    const thumbRequest = new ThumbAssetServeAction({
      thumbAssetId: "1_ep9epsxy"
    });

    kontorolClient.setDefaultRequestOptions({ks: "ks123"});

    expect.assertions(3);

    kontorolClient.request(thumbRequest)
      .subscribe(
        result => {
          asyncAssert(() => {
            expect(result).toBeDefined();
            expect(result.url).toBeDefined();
            expect(result.url).toBe(`https://www.kontorol.com/api_v3/service/thumbasset/action/serve?format=1&clientTag=ngx-tests&ks=ks123&thumbAssetId=1_ep9epsxy&apiVersion=${environment.request.apiVersion}`);
          });

          done();
        },
        error => {
          fail(error);
        });

  });

  test("error when sending 'KontorolFileRequest' as part of multi-request", (done) => {

    const thumbRequest: any = new ThumbAssetServeAction({
      thumbAssetId: "thumbAssetId"
    });

    expect.assertions(3);

    kontorolClient.multiRequest([thumbRequest])
      .subscribe(
        result => {
          done.fail("got response instead of error");
        },
        error => {
          asyncAssert(() => {
            expect(error).toBeDefined();
            expect(error).toBeInstanceOf(Error);
            expect(error.message).toBe("multi-request not support requests of type 'KontorolFileRequest', use regular request instead");
          });
          done();
        });

  });
});
