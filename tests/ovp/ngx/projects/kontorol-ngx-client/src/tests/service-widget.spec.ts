import {WidgetListAction} from "../lib/api/types/WidgetListAction";
import {KontorolWidgetListResponse} from "../lib/api/types/KontorolWidgetListResponse";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolClient} from "../lib/kontorol-client.service";

describe(`service "Widget" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return new Promise((resolve => {
      getClient()
        .subscribe(client => {
          kontorolClient = client;
          resolve(client);
        });
    }));
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test("widgets list", (done) => {
    expect.assertions(1);
    kontorolClient.request(new WidgetListAction())
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response instanceof KontorolWidgetListResponse).toBeTruthy();
          });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });
});
