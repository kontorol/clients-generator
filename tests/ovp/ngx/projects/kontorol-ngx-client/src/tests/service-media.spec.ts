import {KontorolBrowserHttpClient} from "../lib/kontorol-clients/kontorol-browser-http-client";
import {MediaListAction} from "../lib/api/types/MediaListAction";
import {KontorolMediaListResponse} from "../lib/api/types/KontorolMediaListResponse";
import {KontorolMediaEntry} from "../lib/api/types/KontorolMediaEntry";
import {KontorolMediaType} from "../lib/api/types/KontorolMediaType";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolClient} from "../lib/kontorol-client.service";

describe(`service "Media" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return new Promise((resolve => {
      getClient()
        .subscribe(client => {
          kontorolClient = client;
          resolve(client);
        });
    }));
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test(`invoke "list" action`, (done) => {

    if (!kontorolClient) {
      fail(`failure during 'SessionStart'. aborting test`);
      return;
    }

    expect.assertions(4);
    kontorolClient.request(new MediaListAction()).subscribe(
      (response) => {
        asyncAssert(() => {
          expect(response instanceof KontorolMediaListResponse).toBeTruthy();
          expect(response.objects).toBeDefined();
          expect(response.objects instanceof Array).toBeTruthy();
          expect(response.objects[0] instanceof KontorolMediaEntry).toBeTruthy();
        });
        done();
      },
      () => {
        done.fail(`failed to perform request`);
      }
    );
  });

  /*
    def test_createRemote(self):
        mediaEntry = KontorolMediaEntry()
        mediaEntry.setName("pytest.MediaTests.test_createRemote")
        mediaEntry.setMediaType(KontorolMediaType(KontorolMediaType.VIDEO))

        ulFile = getTestFile("DemoVideo.flv")
        uploadTokenId = self.client.media.upload(ulFile)

        mediaEntry = self.client.media.addFromUploadedFile(mediaEntry, uploadTokenId)

        self.assertIsInstance(mediaEntry.getId(), six.text_type)

        #cleanup
        self.client.media.delete(mediaEntry.id)
  */
  xtest(`invoke "createRemote" action`, () => {
    const media = new KontorolMediaEntry({
      name: "typescript.MediaTests.test_createRemote",
      mediaType: KontorolMediaType.video
    });
  });

  describe(`utf-8 tests`, () => {
    /*
      def test_utf8_name(self):
          test_unicode = six.u('\u03dd\xf5\xf6')  #an odd representation of the word 'FOO'
          mediaEntry = KontorolMediaEntry()
          mediaEntry.setName(u'pytest.MediaTests.test_UTF8_name'+test_unicode)
          mediaEntry.setMediaType(KontorolMediaType(KontorolMediaType.VIDEO))
          ulFile = getTestFile('DemoVideo.flv')
          uploadTokenId = self.client.media.upload(ulFile)

          #this will throw an exception if fail.
          mediaEntry = self.client.media.addFromUploadedFile(mediaEntry, uploadTokenId)

          self.addCleanup(self.client.media.delete, mediaEntry.getId())
     */
    xtest(`support utf-8 name`, () => {
      const media = new KontorolMediaEntry({
        name: "typescript.MediaTests.test_UTF8_name" + "\u03dd\xf5\xf6",
        mediaType: KontorolMediaType.video
      });
    });

    /*
      def test_utf8_tags(self):

          test_unicode = u'\u03dd\xf5\xf6'  #an odd representation of the word 'FOO'
          mediaEntry = KontorolMediaEntry()
          mediaEntry.setName('pytest.MediaTests.test_UTF8_tags')
          mediaEntry.setMediaType(KontorolMediaType(KontorolMediaType.VIDEO))
          ulFile = getTestFile('DemoVideo.flv')
          uploadTokenId = self.client.media.upload(ulFile)

          mediaEntry.setTags(test_unicode)

          #this will throw an exception if fail.
          mediaEntry = self.client.media.addFromUploadedFile(mediaEntry, uploadTokenId)

          self.addCleanup(self.client.media.delete, mediaEntry.getId())
     */
    xtest(`support utf-8 tags`, () => {
      const media = new KontorolMediaEntry({
        name: "typescript.MediaTests.test_UTF8_tags",
        mediaType: KontorolMediaType.video
      });
    });
  });
});
