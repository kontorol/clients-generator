import {FlavorAssetListAction} from "../lib/api/types/FlavorAssetListAction";
import {KontorolFlavorAssetListResponse} from "../lib/api/types/KontorolFlavorAssetListResponse";
import {KontorolFlavorAsset} from "../lib/api/types/KontorolFlavorAsset";
import {BaseEntryListAction} from "../lib/api/types/BaseEntryListAction";
import {KontorolMediaEntryFilter} from "../lib/api/types/KontorolMediaEntryFilter";
import {KontorolFlavorAssetFilter} from "../lib/api/types/KontorolFlavorAssetFilter";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolResponse} from "../lib/api/kontorol-response";
import {KontorolClient} from "../lib/kontorol-client.service";

describe(`service "Flavor" tests`, () => {
	let kontorolClient: KontorolClient = null;

	beforeAll(async () => {
		LoggerSettings.logLevel = LogLevels.error; // suspend warnings

		return new Promise((resolve => {
			getClient()
				.subscribe(client => {
					kontorolClient = client;
					resolve(client);
				});
		}));
	});

	afterAll(() => {
		kontorolClient = null;
	});

	test("flavor list", (done) => {
		expect.assertions(3);
		kontorolClient.multiRequest([
			new BaseEntryListAction(
				{
					filter: new KontorolMediaEntryFilter({
						flavorParamsIdsMatchOr: '0'
					})
				}
			),
			new FlavorAssetListAction(
				{
					filter: new KontorolFlavorAssetFilter(
						{
							entryIdEqual: ''
						}
					).setDependency(['entryIdEqual',0,'objects:0:id'])
				}
			)]
		)
			.subscribe(
				responses => {

					const response: KontorolResponse<KontorolFlavorAssetListResponse> = responses[1];
					asyncAssert(() => {
						expect(response.result instanceof KontorolFlavorAssetListResponse).toBeTruthy();
						expect(Array.isArray(response.result.objects)).toBeTruthy();
						expect(response.result.objects.every(obj => obj instanceof KontorolFlavorAsset)).toBeTruthy();
					});
					done();
				},
				(error) => {
					done.fail(error);
				}
			);
	});
});
