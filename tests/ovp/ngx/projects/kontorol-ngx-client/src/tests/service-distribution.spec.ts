import {DistributionProviderListAction} from "../lib/api/types/DistributionProviderListAction";
import {KontorolDistributionProviderListResponse} from "../lib/api/types/KontorolDistributionProviderListResponse";
import {KontorolDistributionProvider} from "../lib/api/types/KontorolDistributionProvider";
import {DistributionProfileListAction} from "../lib/api/types/DistributionProfileListAction";
import {KontorolDistributionProfileListResponse} from "../lib/api/types/KontorolDistributionProfileListResponse";
import {KontorolDistributionProfile} from "../lib/api/types/KontorolDistributionProfile";
import {EntryDistributionListAction} from "../lib/api/types/EntryDistributionListAction";
import {KontorolEntryDistributionListResponse} from "../lib/api/types/KontorolEntryDistributionListResponse";
import {KontorolEntryDistribution} from "../lib/api/types/KontorolEntryDistribution";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolClient} from "../lib/kontorol-client.service";

describe(`service "Distribution" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return new Promise((resolve => {
      getClient()
        .subscribe(client => {
          kontorolClient = client;
          resolve(client);
        });
    }));
  });

  test("distribution provider list", (done) => {
    expect.assertions(3);
    kontorolClient.request(new DistributionProviderListAction())
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response instanceof KontorolDistributionProviderListResponse).toBeTruthy();
            expect(Array.isArray(response.objects)).toBeTruthy();
            expect(response.objects.every(obj => obj instanceof KontorolDistributionProvider)).toBeTruthy();
          });
          done();
        },
        (error) => {
          done.fail(error);
        });
  });

  test("distribution profile list", (done) => {
    expect.assertions(3);
    kontorolClient.request(new DistributionProfileListAction())
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response instanceof KontorolDistributionProfileListResponse).toBeTruthy();
            expect(Array.isArray(response.objects)).toBeTruthy();
            expect(response.objects.every(obj => obj instanceof KontorolDistributionProfile)).toBeTruthy();
          });
          done();
        },
        () => {
          done.fail("should not reach this part");
        });
  });

  test("entry distribution list", (done) => {
    expect.assertions(3);
    kontorolClient.request(new EntryDistributionListAction())
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response instanceof KontorolEntryDistributionListResponse).toBeTruthy();
            expect(Array.isArray(response.objects)).toBeTruthy();
            expect(response.objects.every(obj => obj instanceof KontorolEntryDistribution)).toBeTruthy();
          });
          done();
        },
        () => {
          done.fail("should not reach this part");
        });
  });
});
