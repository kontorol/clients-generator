import {UiConfListAction} from "../lib/api/types/UiConfListAction";
import {KontorolUiConfListResponse} from "../lib/api/types/KontorolUiConfListResponse";
import {KontorolUiConf} from "../lib/api/types/KontorolUiConf";
import {KontorolUiConfFilter} from "../lib/api/types/KontorolUiConfFilter";
import {KontorolUiConfObjType} from "../lib/api/types/KontorolUiConfObjType";
import {UiConfListTemplatesAction} from "../lib/api/types/UiConfListTemplatesAction";
import { asyncAssert, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolClient} from "../lib/kontorol-client.service";

describe(`service "UIConf" tests`, () => {
  let kontorolClient: KontorolClient = null;

  beforeAll(async () => {
    LoggerSettings.logLevel = LogLevels.error; // suspend warnings

    return new Promise((resolve => {
      getClient()
        .subscribe(client => {
          kontorolClient = client;
          resolve(client);
        });
    }));
  });

  afterAll(() => {
    kontorolClient = null;
  });

  test("uiconf list", (done) => {
    expect.assertions(3);
    kontorolClient.request(new UiConfListAction())
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response instanceof KontorolUiConfListResponse).toBeTruthy();
            expect(Array.isArray(response.objects)).toBeTruthy();
            expect(response.objects.every(obj => obj instanceof KontorolUiConf)).toBeTruthy();
          });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });


  // TODO [kmc] investigate response
  xtest("get players", (done) => {
    const players = [KontorolUiConfObjType.player, KontorolUiConfObjType.playerV3, KontorolUiConfObjType.playerSl];
    const filter = new KontorolUiConfFilter({objTypeIn: players.join(",")});
    expect.assertions(1);
    kontorolClient.request(new UiConfListAction(filter))
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response.objects.every(obj => players.indexOf(Number(obj.objType)) !== -1)).toBeTruthy();
          });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });

  test("get video players", (done) => {
    const players = [KontorolUiConfObjType.player, KontorolUiConfObjType.playerV3, KontorolUiConfObjType.playerSl];
    const filter = new KontorolUiConfFilter({
      objTypeIn: players.join(","),
      tagsMultiLikeOr: "player"
    });

    expect.assertions(2);
    kontorolClient.request(new UiConfListAction(filter))
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response.objects).toBeDefined();
            expect(response.objects.length).toBeGreaterThan(0);
            const match = /isPlaylist="(.*?)"/g.exec(response.objects[0].confFile);
            if (match) {
              expect(["true", "multi"].indexOf(match[1]) !== -1).toBeTruthy();
            }
          });
          done();
        },
        (error) => {
          fail(error);
          done();
        }
      );
  });

  test("uiconf list templates", (done) => {
    expect.assertions(3);
    kontorolClient.request(new UiConfListTemplatesAction())
      .subscribe(
        response => {
          asyncAssert(() => {
            expect(response instanceof KontorolUiConfListResponse).toBeTruthy();
            expect(Array.isArray(response.objects)).toBeTruthy();
            expect(response.objects.every(obj => obj instanceof KontorolUiConf)).toBeTruthy();
          });
          done();
        },
        (error) => {
          done.fail(error);
        }
      );
  });
});
