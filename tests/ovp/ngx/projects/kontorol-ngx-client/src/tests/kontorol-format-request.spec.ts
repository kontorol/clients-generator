import {BaseEntryListAction} from "../lib/api/types/BaseEntryListAction";
import {UserLoginByLoginIdAction} from "../lib/api/types/UserLoginByLoginIdAction";
import {KontorolDetachedResponseProfile} from "../lib/api/types/KontorolDetachedResponseProfile";
import {KontorolBaseEntryFilter} from "../lib/api/types/KontorolBaseEntryFilter";
import {KontorolSearchOperator} from "../lib/api/types/KontorolSearchOperator";
import {KontorolNullableBoolean} from "../lib/api/types/KontorolNullableBoolean";
import {AppTokenAddAction} from "../lib/api/types/AppTokenAddAction";
import {KontorolAppToken} from "../lib/api/types/KontorolAppToken";
import {KontorolSearchOperatorType} from "../lib/api/types/KontorolSearchOperatorType";
import {KontorolContentDistributionSearchItem} from "../lib/api/types/KontorolContentDistributionSearchItem";
import {UserGetAction} from "../lib/api/types/UserGetAction";
import {PlaylistListAction} from "../lib/api/types/PlaylistListAction";
import {KontorolResponseType} from "../lib/api/types/KontorolResponseType";
import {KontorolBaseEntryListResponse} from "../lib/api/types/KontorolBaseEntryListResponse";
import {KontorolPlaylist} from "../lib/api/types/KontorolPlaylist";
import {PartnerGetAction} from "../lib/api/types/PartnerGetAction";
import {KontorolPlaylistType} from "../lib/api/types/KontorolPlaylistType";
import {KontorolEntryReplacementStatus} from "../lib/api/types/KontorolEntryReplacementStatus";
import {KontorolMediaEntryFilterForPlaylist} from "../lib/api/types/KontorolMediaEntryFilterForPlaylist";
import {KontorolAPIException} from "../lib/api/kontorol-api-exception";
import {KontorolAppTokenHashType} from "../lib/api/types/KontorolAppTokenHashType";
import {KontorolMediaEntryFilter} from "../lib/api/types/KontorolMediaEntryFilter";
import {KontorolMediaEntry} from "../lib/api/types/KontorolMediaEntry";
import { asyncAssert, escapeRegExp, getClient } from "./utils";
import {LoggerSettings, LogLevels} from "../lib/api/kontorol-logger";
import {KontorolFilterPager} from "../lib/api/types/KontorolFilterPager";
import {KontorolClient} from "../lib/kontorol-client.service";
import {TestsConfig} from './tests-config';

describe("Kontorol server API request", () => {
    let kontorolClient: KontorolClient = null;

    beforeAll(async () => {
        LoggerSettings.logLevel = LogLevels.error; // suspend warnings

        return new Promise((resolve => {
            getClient()
                .subscribe(client => {
                    kontorolClient = client;
                    resolve(client);
                });
        }));
    });

    afterAll(() => {
        kontorolClient = null;
    });

    describe("Kontorol request with specific format type", () => {
        test("handle response format 1 (json)", (done) => {
            // example of assignment by setParameters function (support chaining)
            const listAction: BaseEntryListAction = new BaseEntryListAction(
                {
                    filter: new KontorolBaseEntryFilter().setData(filter => {
                        filter.statusIn = "2";
                    })
                });
            expect.assertions(2);
            kontorolClient.request(listAction, KontorolResponseType.responseTypeJson).subscribe(
                (response) => {
                    asyncAssert(() => {
                        expect(typeof response === 'string').toBeTruthy();
                        expect(JSON.parse(response)).toBeDefined();
                    });
                    done();
                },
                (error) => {
                    done.fail(error);
                }
            );
        });

        test("handle response format 2 (xml)", (done) => {
            // example of assignment by setParameters function (support chaining)
            const listAction: BaseEntryListAction = new BaseEntryListAction(
                {
                    filter: new KontorolBaseEntryFilter().setData(filter => {
                        filter.statusIn = "2";
                    })
                });
            expect.assertions(2);
            kontorolClient.request(listAction, KontorolResponseType.responseTypeXml).subscribe(
                (response) => {
                    asyncAssert(() => {
                        expect(typeof response === 'string').toBeTruthy();
                        expect(response.indexOf('<?xml ')).toBe(0);
                    });
                    done();
                },
                (error) => {
                    done.fail(error);
                }
            );
        });
    });
});
