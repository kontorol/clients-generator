## Kontorol node.js API Client Library.
Compatible with Kontorol OTT server version @VERSION@ and above.

[![NPM](https://nodei.co/npm/kontorol-ott-client.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/kontorol-ott-client/)


You can install this client library using npm with:
```
npm install kontorol-ott-client@@VERSION@
```

## Sanity Check
- Copy config.template.json to config.json and replace tokens
- Run npm test

## Code contributions

We are happy to accept pull requests, please see [contribution guidelines](https://github.com/kontorol/platform-install-packages/blob/master/doc/Contributing-to-the-Kontorol-Platform.md)

The contents of this client are auto generated from https://github.com/kontorol/clients-generator and pull requests should be made there, rather than to the https://github.com/kontorol/KontorolOttGeneratedAPIClientsNode repo.

Relevant files are:
- sources/node2
- tests/ott/node2
- lib/Node2ClientGenerator.php
