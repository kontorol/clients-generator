package utils

import (
	"context"

	"github.com/kontorol/KontorolOttGeneratedAPIClientsGo/kontorolclient"
)

const (
	_requestIdKey string = "test-key-requestId"
)

func GetRequestId(ctx context.Context) (string, bool) {
	val, ok := ctx.Value(_requestIdKey).(string)
	return val, ok
}

func WithRequestId(c context.Context, requestId string) context.Context {
	return context.WithValue(c, _requestIdKey, requestId)
}

func CreateClientAndMock() (*kontorolclient.Client, *MockHttpClient) {
	config := CreateTestConfig()
	mockHttpClient := NewMockHttpClient()
	httpHandler := kontorolclient.NewHttpHandler(kontorolclient.GetBaseUrl(config), mockHttpClient)
	var client *kontorolclient.Client
	client = kontorolclient.NewClient(httpHandler.Execute)

	return client, mockHttpClient
}

func CreateTestConfig() kontorolclient.Configuration {
	return kontorolclient.Configuration{
		ServiceUrl:                "test.com",
		TimeoutMs:                 30000,
		MaxConnectionsPerHost:     1024,
		IdleConnectionTimeoutMs:   30000,
		MaxIdleConnections:        1024,
		MaxIdleConnectionsPerHost: 1024,
	}
}
