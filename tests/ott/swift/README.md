# KontorolOttClient

[![CI Status](http://img.shields.io/travis/kontorol/KontorolOttGeneratedAPIClientsSwift.svg?style=flat)](https://travis-ci.org/kontorol/KontorolOttGeneratedAPIClientsSwift)
[![Version](https://img.shields.io/cocoapods/v/KontorolOttClient.svg?style=flat)](http://cocoapods.org/pods/KontorolOttClient)
[![License](https://img.shields.io/cocoapods/l/KontorolOttClient.svg?style=flat)](http://cocoapods.org/pods/KontorolOttClient)
[![Platform](https://img.shields.io/cocoapods/p/KontorolOttClient.svg?style=flat)](http://cocoapods.org/pods/KontorolOttClient)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KontorolOttClient is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KontorolOttClient"
```

## Author

community@kontorol.com

## License and Copyright Information
All code in this project is released under the [AGPLv3 license](http://www.gnu.org/licenses/agpl-3.0.html) unless a different license for a particular library is specified in the applicable library path.   

Copyright © Kontorol Inc. All rights reserved.   
Authors and contributors: See [GitHub contributors list](https://github.com/kontorol/KontorolOttGeneratedAPIClientsSwift/graphs/contributors).  

## Publishing

```ruby
pod lib lint --fail-fast --allow-warnings
pod trunk push KontorolOttClient.podspec --allow-warnings 
```
