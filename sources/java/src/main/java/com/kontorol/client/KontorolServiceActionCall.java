// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================
package com.kontorol.client;

/**
 * A KontorolServiceActionCall is what the client queues to represent a request to the Kontorol server.
 * 
 * @author jpotts
 *
 */
public class KontorolServiceActionCall {
	private String service;
    private String action;
    private KontorolParams params;
    private KontorolFiles files;
    
    public KontorolServiceActionCall(String service, String action, KontorolParams kparams) {
        this(service, action, kparams, new KontorolFiles());
    }

    public KontorolServiceActionCall(String service, String action, KontorolParams kparams, KontorolFiles kfiles) {
        this.service = service;
        this.action = action;
        this.params = kparams;
        this.files = kfiles;
    }

    public String getService() {
        return this.service;
    }

    public String getAction() {    
    	return this.action;
    }

    public KontorolParams getParams() {
        return this.params;
    }

    public KontorolFiles getFiles() {
        return this.files;
    }

    public KontorolParams getParamsForMultiRequest(int multiRequestNumber) throws KontorolApiException {
        KontorolParams multiRequestParams = new KontorolParams();
        
        params.add("service", service);
        params.add("action", action);
        multiRequestParams.add(Integer.toString(multiRequestNumber), params);
        
        return multiRequestParams;
    }

    public KontorolFiles getFilesForMultiRequest(int multiRequestNumber) {
    	
        KontorolFiles multiRequestFiles = new KontorolFiles();
        multiRequestFiles.add(Integer.toString(multiRequestNumber), files);
        return multiRequestFiles;
    }

}
