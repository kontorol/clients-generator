// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================
package com.kontorol.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.kontorol.client.enums.KontorolEnumAsInt;
import com.kontorol.client.enums.KontorolEnumAsString;

/**
 * Helper class that provides a collection of Kontorol parameters (key-value
 * pairs).
 * 
 * @author jpotts
 * 
 */
public class KontorolParams extends JSONObject implements Serializable  {

	public String toQueryString() throws KontorolApiException {
		return toQueryString(null);
	}

	public String toQueryString(String prefix) throws KontorolApiException {

		StringBuffer str = new StringBuffer();
		Object value;
		String key;
		for (Object keyObject : keySet()) {
			key = (String) keyObject;
			if (str.length() > 0) {
				str.append("&");
			}

			try {
				value = get(key);
			} catch (JSONException e) {
				throw new KontorolApiException(e.getMessage());
			}

			if (prefix != null) {
				key = prefix + "[" + key + "]";
			}
			if (value instanceof KontorolParams) {
				str.append(((KontorolParams) value).toQueryString(key));
			} else {
				str.append(key);
				str.append("=");
				str.append(value);
			}
		}

		return str.toString();
	}

	public void add(String key, int value) throws KontorolApiException {
		if (value == KontorolParamsValueDefaults.KONTOROL_UNDEF_INT) {
			return;
		}

		if (value == KontorolParamsValueDefaults.KONTOROL_NULL_INT) {
			putNull(key);
			return;
		}

		try {
			put(key, value);
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	public void add(String key, long value) throws KontorolApiException {
		if (value == KontorolParamsValueDefaults.KONTOROL_UNDEF_LONG) {
			return;
		}
		if (value == KontorolParamsValueDefaults.KONTOROL_NULL_LONG) {
			putNull(key);
			return;
		}

		try {
			put(key, value);
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	public void add(String key, double value) throws KontorolApiException {
		if (value == KontorolParamsValueDefaults.KONTOROL_UNDEF_DOUBLE) {
			return;
		}
		if (value == KontorolParamsValueDefaults.KONTOROL_NULL_DOUBLE) {
			putNull(key);
			return;
		}

		try {
			put(key, value);
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	public void add(String key, String value) throws KontorolApiException {
		if (value == null) {
			return;
		}

		if (value.equals(KontorolParamsValueDefaults.KONTOROL_NULL_STRING)) {
			putNull(key);
			return;
		}

		try {
			put(key, value);
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	public void add(String key, KontorolObjectBase object)
			throws KontorolApiException {
		if (object == null)
			return;

		try {
			put(key, object.toParams());
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	public <T extends KontorolObjectBase> void add(String key, ArrayList<T> array)
			throws KontorolApiException {
		if (array == null)
			return;

		if (array.isEmpty()) {
			KontorolParams emptyParams = new KontorolParams();
			try {
				emptyParams.put("-", "");
				put(key, emptyParams);
			} catch (JSONException e) {
				throw new KontorolApiException(e.getMessage());
			}
		} else {
			JSONArray arrayParams = new JSONArray();
			for (KontorolObjectBase baseObj : array) {
				arrayParams.put(baseObj.toParams());
			}
			try {
				put(key, arrayParams);
			} catch (JSONException e) {
				throw new KontorolApiException(e.getMessage());
			}
		}
	}

	public <T extends KontorolObjectBase> void add(String key,
			HashMap<String, T> map) throws KontorolApiException {
		if (map == null)
			return;

		if (map.isEmpty()) {
			KontorolParams emptyParams = new KontorolParams();
			try {
				emptyParams.put("-", "");
				put(key, emptyParams);
			} catch (JSONException e) {
				throw new KontorolApiException(e.getMessage());
			}
		} else {
			KontorolParams mapParams = new KontorolParams();
			for (String itemKey : map.keySet()) {
				KontorolObjectBase baseObj = map.get(itemKey);
				mapParams.add(itemKey, baseObj);
			}
			try {
				put(key, mapParams);
			} catch (JSONException e) {
				throw new KontorolApiException(e.getMessage());
			}
		}
	}

	public <T extends KontorolObjectBase> void add(String key,
			KontorolParams params) throws KontorolApiException {
		try {
			if (params instanceof KontorolParams && has(key)
					&& get(key) instanceof KontorolParams) {
				KontorolParams existingParams = (KontorolParams) get(key);
				existingParams.putAll((KontorolParams) params);
			} else {
				put(key, params);
			}
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	public Iterable<String> keySet() {
		return new Iterable<String>() {
			@SuppressWarnings("unchecked")
			public Iterator<String> iterator() {
				return keys();
			}
		};
	}

	private void putAll(KontorolParams params) throws KontorolApiException {
		for (Object key : params.keySet()) {
			String keyString = (String) key;
			try {
				put(keyString, params.get(keyString));
			} catch (JSONException e) {
				throw new KontorolApiException(e.getMessage());
			}
		}
	}

	public void add(KontorolParams objectProperties) throws KontorolApiException {
		putAll(objectProperties);
	}

	protected void putNull(String key) throws KontorolApiException {
		try {
			put(key + "__null", "");
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	/**
	 * Pay attention - this function does not check if the value is null.
	 * neither it supports setting value to null.
	 */
	public void add(String key, boolean value) throws KontorolApiException {
		try {
			put(key, value);
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
	}

	/**
	 * Pay attention - this function does not support setting value to null.
	 */
	public void add(String key, KontorolEnumAsString value)
			throws KontorolApiException {
		if (value == null)
			return;

		add(key, value.getHashCode());
	}

	/**
	 * Pay attention - this function does not support setting value to null.
	 */
	public void add(String key, KontorolEnumAsInt value)
			throws KontorolApiException {
		if (value == null)
			return;

		add(key, value.getHashCode());
	}

	public boolean containsKey(String key) {
		return has(key);
	}

	public void clear() {
		for (Object key : keySet()) {
			remove((String) key);
		}
	}

	public KontorolParams getParams(String key) throws KontorolApiException {
		if (!has(key))
			return null;

		Object value;
		try {
			value = get(key);
		} catch (JSONException e) {
			throw new KontorolApiException(e.getMessage());
		}
		if (value instanceof KontorolParams)
			return (KontorolParams) value;

		throw new KontorolApiException("Key value [" + key
				+ "] is not instance of KontorolParams");
	}

}
