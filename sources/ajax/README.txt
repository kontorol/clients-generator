Kontorol JavaScript API Client Library.

The library contain the following files:
 - example.html
 - jquery-3.1.0.min.js
 - KontorolClient.js - all client functionality without the services.
 - KontorolClient.min.js - KontorolClient.js minified.
 - KontorolFullClient.js - all client functionality including all services.
 - KontorolFullClient.min.js - KontorolFullClient.js minified.
 - Services files, e.g. KontorolAccessControlProfileService.js.
 - Minified services files, e.g. KontorolAccessControlProfileService.min.js.

If you're lazy developer and don't want to include each used service separately, 
or if you find yourself including many services as run time,
you might want to use the single KontorolFullClient.min.js that already contains all services.

If your application is using merely few services, it would be more efficient to include only KontorolClient.min.js
and the minified services files that you need.

