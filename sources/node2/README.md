## Kontorol node.js API Client Library.
Compatible with Kontorol server version @VERSION@ and above.
This client library replaces the older architecture that presented in previous node.js client library.

[![NPM](https://nodei.co/npm/kontorol-client.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/kontorol-client/)


You can install this client library using npm with:
```
npm install kontorol-client 
```
## Proxy settings for client
If the Kontorol client has to be used behind a proxy, this can be set in the KontorolConfiguration by setting proxy
to the url of the proxy. For example:

```js
const config = new kontorol.Configuration();
    
const proxyUrl = new URL('http://some.proxy.com');
proxyUrl.username = 'user';
proxyUrl.password = 'pass';
    
config.proxy = proxyUrl.toString();
const client = new kontorol.Client(config);
```

## Sanity Check
- Copy config.template.json to config.json  and set partnerId, secret and serviceUrl
- Run npm test

## Code contributions

We are happy to accept pull requests, please see [contribution guidelines](https://github.com/kontorol/platform-install-packages/blob/master/doc/Contributing-to-the-Kontorol-Platform.md)

The contents of this client are auto generated from https://github.com/kontorol/clients-generator and pull requests should be made there, rather than to the https://github.com/kontorol/KontorolGeneratedAPIClientsNodeJS repo.

Relevant files are:
- sources/node2
- tests/ovp/node2
- lib/Node2ClientGenerator.php

[![Build Status](https://travis-ci.org/kontorol/KontorolGeneratedAPIClientsNodeJS.svg?branch=master)](https://travis-ci.org/kontorol/KontorolGeneratedAPIClientsNodeJS)
