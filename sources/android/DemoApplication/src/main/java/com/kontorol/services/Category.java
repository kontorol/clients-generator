package com.kontorol.services;

import java.util.List;

import android.util.Log;

import com.kontorol.client.KontorolApiException;
import com.kontorol.client.KontorolClient;
import com.kontorol.client.services.KontorolCategoryService;
import com.kontorol.client.types.KontorolCategory;
import com.kontorol.client.types.KontorolCategoryFilter;
import com.kontorol.client.types.KontorolCategoryListResponse;
import com.kontorol.client.types.KontorolFilterPager;

/**
 * Add & Manage Categories *
 */
public class Category {

    /**
     * Get a list of all categories on the kontorol server
     *
     * @param TAG constant in your class
     * @param pageindex The page number for which {pageSize} of objects should
     * be retrieved (Default is 1)
     * @param pageSize The number of objects to retrieve. (Default is 30,
     * maximum page size is 500)
     *
     * @return The list of all categories
     *
     * @throws KontorolApiException
     */
    public static List<KontorolCategory> listAllCategories(String TAG, int pageIndex, int pageSize) throws KontorolApiException {
        // create a new ADMIN-session client
        KontorolClient client = AdminUser.getClient();//RequestsKontorol.getKontorolClient();

        // create a new mediaService object for our client
        KontorolCategoryService categoryService = client.getCategoryService();

        // create a new filter to filter entries - not mandatory
        KontorolCategoryFilter filter = new KontorolCategoryFilter();
        //filter.mediaTypeEqual = mediaType;

        // create a new pager to choose how many and which entries should be recieved
        // out of the filtered entries - not mandatory
        KontorolFilterPager pager = new KontorolFilterPager();
        pager.pageIndex = pageIndex;
        pager.pageSize = pageSize;

        // execute the list action of the mediaService object to recieve the list of entries
        KontorolCategoryListResponse listResponse = categoryService.list(filter);

        // loop through all entries in the reponse list and print their id.
        Log.w(TAG, "Entries list :");
        int i = 0;
        for (KontorolCategory entry : listResponse.objects) {
            Log.w(TAG, ++i + " id:" + entry.id + " name:" + entry.name + " depth: " + entry.depth + " fullName: " + entry.fullName);
        }
        return listResponse.objects;
    }
}
