/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kontorol.services;

import java.util.List;

import android.util.Log;

import com.kontorol.client.KontorolApiException;
import com.kontorol.client.KontorolClient;
import com.kontorol.client.services.KontorolBaseEntryService;
import com.kontorol.client.services.KontorolFlavorAssetService;
import com.kontorol.client.types.KontorolEntryContextDataParams;
import com.kontorol.client.types.KontorolEntryContextDataResult;
import com.kontorol.client.types.KontorolFilterPager;
import com.kontorol.client.types.KontorolFlavorAsset;
import com.kontorol.client.types.KontorolFlavorAssetFilter;
import com.kontorol.client.types.KontorolFlavorAssetListResponse;

/**
 * Retrieve information and invoke actions on Flavor Asset
 */
public class FlavorAsset {

    /**
     * List Flavor Assets by filter and pager
     *
     * @param TAG constant in your class
     * @param entryId Entry id
     * @param pageindex The page number for which {pageSize} of objects should
     * be retrieved (Default is 1)
     * @param pageSize The number of objects to retrieve. (Default is 30,
     * maximum page size is 500)
     *
     * @return The list of all categories
     *
     * @throws KontorolApiException
     */
    public static List<KontorolFlavorAsset> listAllFlavorAssets(String TAG, String entryId, int pageIndex, int pageSize) throws KontorolApiException {
        // create a new ADMIN-session client
        KontorolClient client = AdminUser.getClient();//RequestsKontorol.getKontorolClient();

        KontorolFlavorAssetService flavorAssetService = client.getFlavorAssetService();

        // create a new filter to filter entries - not mandatory
        KontorolFlavorAssetFilter filter = new KontorolFlavorAssetFilter();
        filter.entryIdEqual = entryId;
        // create a new pager to choose how many and which entries should be recieved
        // out of the filtered entries - not mandatory
        KontorolFilterPager pager = new KontorolFilterPager();
        pager.pageIndex = pageIndex;
        pager.pageSize = pageSize;

        // execute the list action of the mediaService object to recieve the list of entries
        KontorolFlavorAssetListResponse listResponseFlavorAsset = flavorAssetService.list(filter);

        return listResponseFlavorAsset.objects;
    }

    /**
     * Get download URL for the asset
     *
     * @param TAG constant in your class
     * @param id asset id
     *
     * @return The asset url
     */
    public static String getUrl(String TAG, String id) throws KontorolApiException {
        // create a new ADMIN-session client
        KontorolClient client = AdminUser.getClient();//RequestsKontorol.getKontorolClient();

        // create a new mediaService object for our client
        KontorolFlavorAssetService mediaService = client.getFlavorAssetService();
        String url = mediaService.getUrl(id);
        Log.w(TAG, "URL for the asset: " + url);
        return url;
    }
    
    /**
     * Return flavorAsset lists from getContextData call
     * @param TAG
     * @param entryId
     * @param flavorTags
     * @return
     * @throws KontorolApiException
     */
    public static List<KontorolFlavorAsset> listAllFlavorsFromContext(String TAG, String entryId, String flavorTags) throws KontorolApiException {
    	 // create a new ADMIN-session client
        KontorolClient client = AdminUser.getClient();//RequestsKontorol.getKontorolClient();

        KontorolEntryContextDataParams params = new KontorolEntryContextDataParams();
        params.flavorTags = flavorTags;
        KontorolBaseEntryService baseEntryService = client.getBaseEntryService();
        KontorolEntryContextDataResult res = baseEntryService.getContextData(entryId, params);
        return res.flavorAssets;
    }
}
