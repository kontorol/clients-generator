package com.kontorol.utils;

import java.util.Comparator;

import com.kontorol.client.types.KontorolCategory;
import com.kontorol.client.types.KontorolFlavorAsset;
import com.kontorol.client.types.KontorolMediaEntry;

/**
 * The class performs a sort
 */
public class Sort<T> implements Comparator<T> {

    private String filter = "name";
    private String direction = "compareTo";

    /**
     * Constructor Description of Sort<T>
     *
     * @param filter Specify which field to sort
     * @param direction Specifies the sort direction
     */
    public Sort(String filter, String direction) {
        this.filter = filter;
        this.direction = direction;
    }

    /**
     * Compares its two arguments for order. Returns a negative integer, zero,
     * or a positive integer as the first argument is less than, equal to, or
     * greater than the second.
     *
     * @param paramT1 the first object to be compared.
     * @param paramT2 the second object to be compared.
     *
     * @return a negative integer, zero, or a positive integer as the first
     * argument is less than, equal to, or greater than the second.
     *
     * @throws ClassCastException - if the arguments' types prevent them from
     * being compared by this Comparator.
     */
    @Override
    public int compare(T paramT1, T paramT2) {

        int res = 0;
        if (paramT1 instanceof KontorolMediaEntry && paramT2 instanceof KontorolMediaEntry) {
            if (this.filter.equals("name")) {
                res = ((KontorolMediaEntry) paramT1).name.compareTo(((KontorolMediaEntry) paramT2).name);
            }
            if (this.filter.equals("plays") && this.direction.equals("compareTo")) {
                res = new Integer(((KontorolMediaEntry) paramT1).plays).compareTo(new Integer(((KontorolMediaEntry) paramT2).plays));
            } else {
                res = ((KontorolMediaEntry) paramT2).plays - ((KontorolMediaEntry) paramT1).plays;
            }
            if (this.filter.equals("createdAt")) {
                res = new Integer(((KontorolMediaEntry) paramT1).createdAt).compareTo(new Integer(((KontorolMediaEntry) paramT2).createdAt));
            }
        }
        if (paramT1 instanceof KontorolCategory && paramT2 instanceof KontorolCategory) {
            res = ((KontorolCategory) paramT1).name.compareTo(((KontorolCategory) paramT2).name);
        }
        if (paramT1 instanceof KontorolFlavorAsset && paramT2 instanceof KontorolFlavorAsset) {
            res = ((KontorolFlavorAsset) paramT2).bitrate - ((KontorolFlavorAsset) paramT1).bitrate;
        }
        return res;
    }
}
