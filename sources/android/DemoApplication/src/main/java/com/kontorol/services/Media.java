package com.kontorol.services;

import java.io.File;
import java.util.List;

import android.util.Log;

import com.kontorol.client.KontorolApiException;
import com.kontorol.client.KontorolClient;
import com.kontorol.client.enums.KontorolEntryType;
import com.kontorol.client.enums.KontorolMediaType;
import com.kontorol.client.services.KontorolMediaService;
import com.kontorol.client.types.KontorolBaseEntry;
import com.kontorol.client.types.KontorolFilterPager;
import com.kontorol.client.types.KontorolMediaEntry;
import com.kontorol.client.types.KontorolMediaEntryFilter;
import com.kontorol.client.types.KontorolMediaListResponse;

/**
 * Media service lets you upload and manage media files (images / videos &
 * audio)
 */
public class Media {

    /**
     * Get a list of all media data from the kontorol server
     *
     * @param TAG constant in your class
     * @param mediaType Type of entries
     * @param pageSize The number of objects to retrieve. (Default is 30,
     * maximum page size is 500)
     *
     * @throws KontorolApiException
     */
    public static List<KontorolMediaEntry> listAllEntriesByIdCategories(String TAG, KontorolMediaEntryFilter filter, int pageIndex, int pageSize) throws KontorolApiException {
        // create a new ADMIN-session client
        KontorolClient client = AdminUser.getClient();//RequestsKontorol.getKontorolClient();

        // create a new mediaService object for our client
        KontorolMediaService mediaService = client.getMediaService();

        // create a new pager to choose how many and which entries should be recieved
        // out of the filtered entries - not mandatory
        KontorolFilterPager pager = new KontorolFilterPager();
        pager.pageIndex = pageIndex;
        pager.pageSize = pageSize;

        // execute the list action of the mediaService object to recieve the list of entries
        KontorolMediaListResponse listResponse = mediaService.list(filter, pager);

        // loop through all entries in the reponse list and print their id.
        Log.w(TAG, "Entries list :");
        int i = 0;
        for (KontorolMediaEntry entry : listResponse.objects) {
            Log.w(TAG, ++i + " id:" + entry.id + " name:" + entry.name + " type:" + entry.type + " dataURL: " + entry.dataUrl);
        }
        return listResponse.objects;
    }

    /**
     * Get media entry by ID
     *
     * @param TAG constant in your class
     * @param entryId Media entry id
     *
     * @return Information about the entry
     *
     * @throws KontorolApiException
     */
    public static KontorolMediaEntry getEntrybyId(String TAG, String entryId) throws KontorolApiException {
        // create a new ADMIN-session client
        KontorolClient client = AdminUser.getClient();//RequestsKontorol.getKontorolClient();

        // create a new mediaService object for our client
        KontorolMediaService mediaService = client.getMediaService();
        KontorolMediaEntry entry = mediaService.get(entryId);
        Log.w(TAG, "Entry:");
        Log.w(TAG, " id:" + entry.id + " name:" + entry.name + " type:" + entry.type + " categories: " + entry.categories);
        return entry;
    }

    /**
     * Creates an empty media entry and assigns basic metadata to it.
     *
     * @param TAG constant in your class
     * @param category Category name which belongs to an entry
     * @param name Name of an entry
     * @param description Description of an entry
     * @param tag Tag of an entry
     *
     * @return Information about created the entry
     *
     *
     */
    public static KontorolMediaEntry addEmptyEntry(String TAG, String category, String name, String description, String tag) {

        try {
            KontorolClient client = AdminUser.getClient();

            Log.w(TAG, "\nCreating an empty Kontorol Entry (without actual media binary attached)...");

            KontorolMediaEntry entry = new KontorolMediaEntry();
            entry.mediaType = KontorolMediaType.VIDEO;
            entry.categories = category;
            entry.name = name;
            entry.description = description;
            entry.tags = tag;

            KontorolMediaEntry newEntry = client.getMediaService().add(entry);
            Log.w(TAG, "\nThe id of our new Video Entry is: " + newEntry.id);
            return newEntry;
        } catch (KontorolApiException e) {
            e.printStackTrace();
            Log.w(TAG, "err: " + e.getMessage());
            return null;
        }
    }

    /**
     * Create an entry
     *
     * @param TAG constant in your class
     * @param String fileName File to upload.
     * @param String entryName Name for the new entry.
     *
     * @throws KontorolApiException
     */
    public static void addEntry(String TAG, String fileName, String entryName) throws KontorolApiException {
        // create a new USER-session client
        KontorolClient client = AdminUser.getClient();

        // upload the new file and recieve the token that identifies it on the kontorol server
        File up = new File(fileName);
        String token = client.getBaseEntryService().upload(up);

        // create a new entry object with the required meta-data
        KontorolBaseEntry entry = new KontorolBaseEntry();
        entry.name = entryName;
        entry.categories = "Comedy";
        entry.type = KontorolEntryType.MEDIA_CLIP;

        // add the entry you created to the kontorol server, by attaching it with the uploaded file
        KontorolBaseEntry newEntry = client.getBaseEntryService().addFromUploadedFile(entry, token);

        // newEntry now contains the information of the new entry that was just created on the server
        Log.w(TAG, "New entry created successfuly with ID " + newEntry.id);
    }
}
