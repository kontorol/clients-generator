
Package contents
=================
 - The Kontorol client library base (KontorolClientBase, KontorolObjectBase...)
 - Auto generated core APIs (KontorolClient...)
 - Required JAR files
 - Project files
 - Library test code and data files (KontorolClientTester/*)
 - Reference application (DemoApplication/*)

Running the test code
======================
1. Import the projects into Eclipse - 
	a. right click in the Package Explorer
	b. Import...
	c. Android->Existing Android Code Into Workspace
	d. Select the root dir containing all 3 android projects (KontorolClient, KontorolClientTester and DemoApplication)
	e. Make sure all 3 projects are selected, click ok
	f. Wait until the projects are automatically compiled (initially some errors will appear, 
		until the KontorolClient is compiled, they should go away automatically)
2. Edit KontorolClientTester/src/com.kontorol.client.test/KontorolTestConfig and fill out your Kontorol account information
3. Right click on KontorolClientTester/src/com.kontorol.client.test/KontorolTestSuite
4. Run As->Android JUnit Test


Running the demo application
=============================
1. Import the projects into Eclipse (see above)
2. Edit Kontorol/src/com.kontorol.activity/Settings.java
3. Search for etEmail.setText and etPassword.setText
4. Set the default user / password to the credentials of you Kontorol KMC account
5. Hit the play button
