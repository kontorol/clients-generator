import { Injectable, InjectionToken } from '@angular/core';

export const KONTOROL_CLIENT_OPTIONS: InjectionToken<KontorolClientOptions> = new InjectionToken('kontorol client options');

export interface KontorolClientOptions {
    clientTag: string;
    endpointUrl: string;
    chunkFileSize?: number;
    chunkFileDisabled?: boolean;
}
