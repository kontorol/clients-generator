import {map, catchError} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { KontorolMultiRequest } from '../api/kontorol-multi-request';
import { KontorolMultiResponse } from '../api/kontorol-multi-response';
import { Observable } from 'rxjs';
import { createClientTag, createEndpoint, getHeaders, prepareParameters } from './utils';
import { KontorolAPIException } from '../api/kontorol-api-exception';
import { KontorolClientException } from '../api/kontorol-client-exception';
import { KontorolRequestOptions } from '../api/kontorol-request-options';
import { KontorolClientOptions } from '../kontorol-client-options';

export class KontorolMultiRequestAdapter {
    constructor(private _http: HttpClient) {
    }

    transmit(request: KontorolMultiRequest,  clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions): Observable<KontorolMultiResponse> {

        const parameters = prepareParameters(request, clientOptions, defaultRequestOptions);

      const endpointOptions = { ...clientOptions, service: parameters['service'], action:  parameters['action'] }
        const endpointUrl = createEndpoint(request, endpointOptions);
        delete parameters['service'];
        delete parameters['action'];

        return this._http.request('post', endpointUrl,
            {
                body: parameters,
                headers: getHeaders()
            }).pipe(
            catchError(
                error => {
                    const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                    throw new KontorolClientException("client::multi-request-network-error", errorMessage || 'Error connecting to server');
                }
            ),
            map(
                result => {
                    try {
                        return request.handleResponse(result);
                    } catch (error) {
                        if (error instanceof KontorolClientException || error instanceof KontorolAPIException) {
                            throw error;
                        } else {
                            const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                            throw new KontorolClientException('client::multi-response-unknown-error', errorMessage || 'Failed to parse response');
                        }
                    }
                }));
    }
}
