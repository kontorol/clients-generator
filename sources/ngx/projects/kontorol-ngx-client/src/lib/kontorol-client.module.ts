import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { KontorolClient } from './kontorol-client.service';
import { HttpClientModule } from '@angular/common/http';
import { KONTOROL_CLIENT_OPTIONS, KontorolClientOptions } from './kontorol-client-options';
import { KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS, KontorolRequestOptionsArgs } from './api/kontorol-request-options';


@NgModule({
    imports: <any[]>[
        HttpClientModule
    ],
    declarations: <any[]>[
    ],
    exports: <any[]>[
    ],
    providers: <any[]>[
    ]
})
export class KontorolClientModule {

    constructor(@Optional() @SkipSelf() module: KontorolClientModule) {
        if (module) {
            throw new Error("'KontorolClientModule' module imported twice.");
        }
    }

    static forRoot(clientOptionsFactory?: () => KontorolClientOptions, defaultRequestOptionsArgsFactory?: () => KontorolRequestOptionsArgs): ModuleWithProviders {
        return {
            ngModule: KontorolClientModule,
            providers: [
                KontorolClient,
                KONTOROL_CLIENT_OPTIONS ? {
                    provide: KONTOROL_CLIENT_OPTIONS,
                    useFactory: clientOptionsFactory
                } : [],
                KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS? {
                    provide: KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS,
                    useFactory: defaultRequestOptionsArgsFactory
                } : []
            ]
        };
    }
}
