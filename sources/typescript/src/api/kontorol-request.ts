import { KontorolResponse } from "./kontorol-response";
import { KontorolRequestBase, KontorolRequestBaseArgs } from "./kontorol-request-base";
import { KontorolAPIException } from './kontorol-api-exception';
import { KontorolObjectBase } from './kontorol-object-base';
import { KontorolRequestOptions, KontorolRequestOptionsArgs } from './kontorol-request-options';
import { environment } from '../environment';

export interface KontorolRequestArgs extends KontorolRequestBaseArgs
{

}


export abstract class KontorolRequest<T> extends KontorolRequestBase {

    private __requestOptions__: KontorolRequestOptions;
    protected callback: (response: KontorolResponse<T>) => void;
    private responseType : string;
    private responseSubType : string;
    protected _responseConstructor : { new() : KontorolObjectBase}; // NOTICE: this property is not used directly. It is here to force import of that type for bundling issues.

    constructor(data : KontorolRequestBaseArgs, {responseType, responseSubType, responseConstructor} : {responseType : string, responseSubType? : string, responseConstructor : { new() : KontorolObjectBase}  } ) {
        super(data);
        this.responseSubType = responseSubType;
        this.responseType = responseType;
        this._responseConstructor = responseConstructor;
    }

    setCompletion(callback: (response: KontorolResponse<T>) => void): this {
        this.callback = callback;
        return this;
    }

    private _unwrapResponse(response: any): any {
        if (environment.response.nestedResponse) {
            if (response && response.hasOwnProperty('result')) {
                if (response.result.hasOwnProperty('error')) {
                    return response.result.error;
                } else {
                    return response.result;
                }
            } else if (response && response.hasOwnProperty('error')) {
                return response.error;
            }
        }

        return response;
    }

    handleResponse(response: any): KontorolResponse<T> {
        let responseResult: any;
        let responseError: any;

        try {
            const unwrappedResponse = this._unwrapResponse(response);
            let responseObject = null;

            if (unwrappedResponse) {
                if (unwrappedResponse instanceof KontorolAPIException)
                {
                    // handle situation when multi request propagated actual api exception object.
                    responseObject = unwrappedResponse;
                }else if (unwrappedResponse.objectType === 'KontorolAPIException') {
                    responseObject = new KontorolAPIException(
                        unwrappedResponse.message,
                        unwrappedResponse.code,
                        unwrappedResponse.args
                    );
                } else {
                    responseObject = super._parseResponseProperty(
                        "",
                        {
                            type: this.responseType,
                            subType: this.responseSubType
                        },
                        unwrappedResponse
                    );
                }
            }

            if (!responseObject && this.responseType !== 'v') {
                responseError = new KontorolAPIException(`server response is undefined, expected '${this.responseType} / ${this.responseSubType}'`, 'client::response_type_error', null);
            } else if (responseObject instanceof KontorolAPIException) {
                // got exception from library
                responseError = responseObject;
            }else {
                responseResult = responseObject;
            }
        } catch (ex) {
            responseError = new KontorolAPIException(ex.message, 'client::general_error', null);
        }


        const result = new KontorolResponse<T>(responseResult, responseError);

        if (this.callback) {
            try {
                this.callback(result);
            } catch (ex) {
                // do nothing by design
            }
        }

        return result;
    }

    setRequestOptions(optionArgs: KontorolRequestOptionsArgs): this;
    setRequestOptions(options: KontorolRequestOptions): this;
    setRequestOptions(arg: KontorolRequestOptionsArgs | KontorolRequestOptions): this {
        this.__requestOptions__ = arg instanceof KontorolRequestOptions ? arg : new KontorolRequestOptions(arg);
        return this;
    }

    getRequestOptions(): KontorolRequestOptions {
        return this.__requestOptions__;
    }

    buildRequest(defaultRequestOptions: KontorolRequestOptions): {} {
        const requestOptionsObject = this.__requestOptions__ ? this.__requestOptions__.toRequestObject() : {};
        const defaultRequestOptionsObject = defaultRequestOptions ? defaultRequestOptions.toRequestObject() : {};

        return Object.assign(
            {},
            defaultRequestOptionsObject,
            requestOptionsObject,
            this.toRequestObject()
        );
    }
}
