import { KontorolObjectBase, KontorolObjectBaseArgs } from './kontorol-object-base';
import { KontorolRequestOptions, KontorolRequestOptionsArgs } from './kontorol-request-options';


export interface KontorolRequestBaseArgs  extends KontorolObjectBaseArgs {
}


export class KontorolRequestBase extends KontorolObjectBase {

    private _networkTag: string;

    constructor(data: KontorolRequestBaseArgs) {
        super(data);
    }

    setNetworkTag(tag: string): this {
        if (!tag || tag.length > 10) {
            console.warn(`cannot set network tag longer than 10 characters. ignoring tag '${tag}`);
        } else {
            this._networkTag = tag;
        }

        return this;
    }

    getNetworkTag(): string {
        return this._networkTag;
    }
}

