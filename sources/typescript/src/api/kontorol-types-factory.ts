import { KontorolObjectBase } from './kontorol-object-base';

// TODO [kontorol] constructor type should be 'KontorolObjectBase' (currently any to support enum of type string)
export type KontorolObjectClass = { new(...args) : any};
const typesMapping : { [key : string] : KontorolObjectClass} = {};

export class KontorolTypesFactory
{
	static registerType(typeName : string, objectCtor :KontorolObjectClass) : void
	{
		typesMapping[typeName] = objectCtor;
	}

	static createObject(type : KontorolObjectBase) : KontorolObjectBase;
	static createObject(typeName : string) : KontorolObjectBase;
	static createObject(type : any) : KontorolObjectBase
	{
		let typeName = '';

		if (type instanceof KontorolObjectBase)
		{
			typeName = type.getTypeName();
		}else if(typeof type === 'string')
		{
			typeName = type;
		}

		const factory : KontorolObjectClass = typeName ? typesMapping[typeName] : null;
		return factory ? new factory() : null;
	}
}
