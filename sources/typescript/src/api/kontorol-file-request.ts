import { KontorolObjectBase } from "./kontorol-object-base";
import { KontorolRequestBase, KontorolRequestBaseArgs } from './kontorol-request-base';
import { KontorolRequest, KontorolRequestArgs } from './kontorol-request';



export interface KontorolFileRequestArgs extends KontorolRequestArgs  {
}

export class KontorolFileRequest extends KontorolRequest<{url: string}> {

    constructor(data: KontorolFileRequestArgs) {
        super(data, {responseType : 'v', responseSubType : '', responseConstructor : null });
    }

}
