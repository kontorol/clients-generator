export interface KontorolClientOptions {
    clientTag: string;
    endpointUrl: string;
    chunkFileSize?: number;
    chunkFileDisabled?: boolean;
}
