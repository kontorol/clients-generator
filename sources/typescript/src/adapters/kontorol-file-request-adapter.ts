import { KontorolFileRequest } from '../api/kontorol-file-request';
import { KontorolRequestOptions, KontorolRequestOptionsArgs } from '../api/kontorol-request-options';
import { buildQuerystring, createClientTag, createEndpoint, prepareParameters } from './utils';
import { KontorolClientOptions } from '../kontorol-client-options';
import { CancelableAction } from '../cancelable-action';


export class KontorolFileRequestAdapter {

    public transmit(request: KontorolFileRequest, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions): CancelableAction<{ url: string }> {
        const parameters = prepareParameters(request, clientOptions, defaultRequestOptions);
        const { service, action, ...queryparams } = parameters;
        const endpointUrl = createEndpoint(request, clientOptions, service, action, queryparams);

        return CancelableAction.resolve({url: endpointUrl});
    }
}
