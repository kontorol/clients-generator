import { KontorolRequest } from '../api/kontorol-request';
import { KontorolAPIException } from '../api/kontorol-api-exception';
import { KontorolClientException } from '../api/kontorol-client-exception';
import { KontorolRequestOptions } from '../api/kontorol-request-options';
import { KontorolClientOptions } from '../kontorol-client-options';
import { createCancelableAction, createEndpoint, getHeaders, prepareParameters } from './utils';
import { CancelableAction } from '../cancelable-action';

export class KontorolRequestAdapter {

    constructor() {
    }

    public transmit<T>(request: KontorolRequest<T>, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions): CancelableAction<T> {

        const parameters = prepareParameters(request, clientOptions, defaultRequestOptions);

        const { service, action, ...body } = parameters;

        const endpoint = createEndpoint(request, clientOptions, service, action);

        return <any>createCancelableAction<T>({endpoint, headers: getHeaders(), body})
            .then(result => {
                    try {
                        const response = request.handleResponse(result);

                        if (response.error) {
                            throw response.error;
                        } else {
                            return response.result;
                        }
                    } catch (error) {
                        if (error instanceof KontorolClientException || error instanceof KontorolAPIException) {
                            throw error;
                        } else {
                            const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                            throw new KontorolClientException('client::response-unknown-error', errorMessage || 'Failed to parse response');
                        }
                    }
                },
                error => {
                    const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                    throw new KontorolClientException("client::request-network-error", errorMessage || 'Error connecting to server');
                });
    }
}
