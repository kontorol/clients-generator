import { KontorolRequestBase } from '../api/kontorol-request-base';
import { KontorolClientOptions } from '../kontorol-client-options';
import { KontorolRequestOptions, KontorolRequestOptionsArgs } from '../api/kontorol-request-options';
import { KontorolMultiRequest } from '../api/kontorol-multi-request';
import { KontorolRequest } from '../api/kontorol-request';
import { KontorolFileRequest } from '../api/kontorol-file-request';
import { CancelableAction } from '../cancelable-action';
import { KontorolAPIException } from '../api/kontorol-api-exception';
import { KontorolClientException } from '../api/kontorol-client-exception';
import { environment } from '../environment';


export function  createEndpoint(request: KontorolRequestBase, options: KontorolClientOptions, service: string, action?: string, additionalQueryparams?: {} ): string {
	const endpoint = options.endpointUrl;
	const clientTag = createClientTag(request, options);
	let url = `${endpoint}/api_v3/service/${service}`;

	if (action) {
		url += `/action/${action}`;
	}

	const queryparams = {
		...(additionalQueryparams || {}),
		...(clientTag ? {clientTag} : {})
	};

	return buildUrl(url, queryparams);
}

export function buildUrl(url: string, querystring?: {}) {
	let formattedUrl = (url).trim();
	if (!querystring) {
		return formattedUrl;
	}
	const urlHasQuerystring = formattedUrl.indexOf('?') !== -1;
	const formattedQuerystring = _buildQuerystring(querystring);
	return `${formattedUrl}${urlHasQuerystring ? '&' : '?'}${formattedQuerystring}`;
}

function _buildQuerystring(data: {}, prefix?: string) {
	let str = [], p;
	for (p in data) {
		if (data.hasOwnProperty(p)) {
			let k = prefix ? prefix + "[" + p + "]" : p, v = data[p];
			str.push((v !== null && typeof v === "object") ?
				_buildQuerystring(v, k) :
				encodeURIComponent(k) + "=" + encodeURIComponent(v));
		}
	}
	return str.join("&");

}

export function createClientTag(request: KontorolRequestBase, options: KontorolClientOptions)
{
    const networkTag = (request.getNetworkTag() || "").trim();
    const clientTag = (options.clientTag || "").trim() || "ng-app";

    if (networkTag && networkTag.length)
    {
        return `${clientTag}_${networkTag}`;
    }else {
        return clientTag;
    }
}

export function buildQuerystring(data: {}, prefix?: string) {
    let str = [], p;
    for (p in data) {
        if (data.hasOwnProperty(p)) {
            let k = prefix ? prefix + "[" + p + "]" : p, v = data[p];
            str.push((v !== null && typeof v === "object") ?
                buildQuerystring(v, k) :
                encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");

}

export function getHeaders(): any {
    return {
        "Accept": "application/json",
        "Content-Type": "application/json"
    };
}

export function prepareParameters(request: KontorolRequest<any> | KontorolMultiRequest | KontorolFileRequest,  options: KontorolClientOptions,  defaultRequestOptions: KontorolRequestOptions): any {

    return Object.assign(
        {},
        request.buildRequest(defaultRequestOptions),
        {
	        apiVersion: environment.request.apiVersion,
            format: 1
        }
    );
}

export function createCancelableAction<T>(data : { endpoint : string, headers : any, body : any} ) : CancelableAction<T> {
	const result = new CancelableAction<T>((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		let isComplete = false;

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				if (isComplete) {
					return;
				}
				isComplete = true;

				let resp;

				try {
					if (xhr.status === 200) {
						resp = JSON.parse(xhr.response);
					} else {
						resp = new KontorolClientException('client::requre-failure', xhr.responseText || 'failed to transmit request');
					}
				} catch (e) {
					resp = new Error(xhr.responseText);
				}

				if (resp instanceof Error || resp instanceof KontorolAPIException) {
					reject(resp);
				} else {
					resolve(resp);
				}
			}
		};

		xhr.open('POST', data.endpoint);

		if (data.headers) {
			Object.keys(data.headers).forEach(headerKey => {
				const headerValue = data.headers[headerKey];
				xhr.setRequestHeader(headerKey, headerValue);
			});
		}

		xhr.send(JSON.stringify(data.body));

		return () => {
			if (!isComplete) {
				isComplete = true;
				xhr.abort();
			}
		};
	});

	return result;
}
