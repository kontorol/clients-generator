import { KontorolMultiRequest } from '../api/kontorol-multi-request';
import { KontorolMultiResponse } from '../api/kontorol-multi-response';
import { createCancelableAction, createEndpoint, getHeaders, prepareParameters } from './utils';
import { KontorolAPIException } from '../api/kontorol-api-exception';
import { KontorolClientException } from '../api/kontorol-client-exception';
import { KontorolRequestOptions } from '../api/kontorol-request-options';
import { KontorolClientOptions } from '../kontorol-client-options';
import { CancelableAction } from '../cancelable-action';

export class KontorolMultiRequestAdapter {
    constructor() {
    }

    transmit(request: KontorolMultiRequest, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions): CancelableAction<KontorolMultiResponse> {

        const parameters = prepareParameters(request, clientOptions, defaultRequestOptions);

        const { service, action, ...body } = parameters;
        const endpoint = createEndpoint(request, clientOptions, service, action);

        return <any>(createCancelableAction<KontorolMultiResponse>({endpoint, headers: getHeaders(), body})
            .then(result => {
                    try {
                        return request.handleResponse(result);
                    } catch (error) {
                        if (error instanceof KontorolClientException || error instanceof KontorolAPIException) {
                            throw error;
                        } else {
                            const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                            throw new KontorolClientException('client::multi-response-unknown-error', errorMessage || 'Failed to parse response');
                        }
                    }
                },
                error => {
                    const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                    throw new KontorolClientException("client::multi-request-network-error", errorMessage || 'Error connecting to server');
                }));
    }
}
