### Submitting a change

This library is auto-generated using `kontorol/clients-generator` php engine. Feel free to clone, build and play with this library but in order to submit PR you should work against the [kontorol/clients-generator](https://github.com/kontorol/clients-generator) repo. 

If your fix is not complex you can open an issue or even a PR and we will consider adding it on our own to the generator. 
