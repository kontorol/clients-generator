import { of as observableOf, Observable } from 'rxjs';
import { KontorolFileRequest } from '../api/kontorol-file-request';
import { KontorolRequestOptions } from '../api/kontorol-request-options';
import { buildUrl, createEndpoint, prepareParameters } from './utils';
import { KontorolClientOptions } from '../kontorol-client-options';
import { environment } from '../environment';


export class KontorolFileRequestAdapter {

  public transmit(request: KontorolFileRequest, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions): Observable<{ url: string }> {
    const parameters = prepareParameters(request, clientOptions, defaultRequestOptions);
    const endpointOptions = {
      ...clientOptions,
      service: parameters['service'],
      action: parameters['action'],
      avoidClientTag: environment.request.avoidQueryString
    }
    const endpointUrl = createEndpoint(request, endpointOptions);
    delete parameters['service'];
    delete parameters['action'];

    return observableOf({url: buildUrl(endpointUrl, parameters)});
  }
}
