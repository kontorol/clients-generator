import { KontorolResponse } from "./kontorol-response";
import { KontorolRequest } from "./kontorol-request";
import { KontorolRequestBase } from "./kontorol-request-base";

import { KontorolMultiResponse } from "./kontorol-multi-response";
import { KontorolAPIException } from "./kontorol-api-exception";
import { KontorolObjectMetadata } from './kontorol-object-base';
import { KontorolRequestOptions } from './kontorol-request-options';
import { environment } from '../environment';
import { createClientTag } from '../adapters/utils';


export class KontorolMultiRequest extends KontorolRequestBase {

    protected callback: (response: KontorolMultiResponse) => void;

    requests: KontorolRequest<any>[] = [];

    constructor(...args: KontorolRequest<any>[]) {
        super({});
        this.requests = args;
    }

    buildRequest(defaultRequestOptions: KontorolRequestOptions | null, clientTag?: string): {} {
        const result = super.toRequestObject();

      if (environment.request.avoidQueryString) {
        result['clientTag'] = createClientTag(this, clientTag);
      }

      for (let i = 0, length = this.requests.length; i < length; i++) {
            result[i] = this.requests[i].buildRequest(defaultRequestOptions, clientTag);
        }

        return result;
    }

    protected _getMetadata() : KontorolObjectMetadata
    {
        const result = super._getMetadata();
        Object.assign(
            result.properties,
            {
                service : { default : 'multirequest', type : 'c'  }
            });

        return result;

    }

    private _unwrapResponse(response: any): any {
        if (environment.response.nestedResponse) {
            if (response && response.hasOwnProperty('result')) {
                return response.result;
            } else if (response && response.hasOwnProperty('error')) {
                return response.error;
            }
        }

        return response;
    }

    setCompletion(callback: (response: KontorolMultiResponse) => void): KontorolMultiRequest {
        this.callback = callback;
        return this;
    }

    handleResponse(responses: any): KontorolMultiResponse {
        const kontorolResponses: KontorolResponse<any>[] = [];

        const unwrappedResponse = this._unwrapResponse(responses);
        let responseObject = null;

        if (!unwrappedResponse || !(unwrappedResponse instanceof Array) || unwrappedResponse.length !== this.requests.length) {
            const response = new KontorolAPIException(`server response is invalid, expected array of ${this.requests.length}`, 'client::response_type_error', null);
            for (let i = 0, len = this.requests.length; i < len; i++) {
                kontorolResponses.push(this.requests[i].handleResponse(response));
            }
        }
        else {

            for (let i = 0, len = this.requests.length; i < len; i++) {
                const serverResponse = unwrappedResponse[i];
                kontorolResponses.push(this.requests[i].handleResponse(serverResponse));
            }

            if (this.callback) {
                try {
                    this.callback(new KontorolMultiResponse(kontorolResponses));
                } catch (ex) {
                    // do nothing by design
                }
            }
        }

        return new KontorolMultiResponse(kontorolResponses);
    }
}
