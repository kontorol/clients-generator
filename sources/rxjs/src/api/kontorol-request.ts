import { KontorolResponse } from "./kontorol-response";
import { KontorolRequestBase, KontorolRequestBaseArgs } from "./kontorol-request-base";
import { KontorolAPIException } from './kontorol-api-exception';
import { KontorolObjectBase } from './kontorol-object-base';
import { KontorolRequestOptions, KontorolRequestOptionsArgs } from './kontorol-request-options';
import { environment } from '../environment';
import { createClientTag } from '../adapters/utils';

export interface KontorolRequestArgs extends KontorolRequestBaseArgs
{

}


export abstract class KontorolRequest<T> extends KontorolRequestBase {

  private __requestOptions__: KontorolRequestOptions;
  protected callback: (response: KontorolResponse<T>) => void;
  private responseType : string;
  private responseSubType : string;
  protected _responseConstructor : { new() : KontorolObjectBase}; // NOTICE: this property is not used directly. It is here to force import of that type for bundling issues.

  constructor(data : KontorolRequestBaseArgs, {responseType, responseSubType, responseConstructor} : {responseType : string, responseSubType? : string, responseConstructor : { new() : KontorolObjectBase}  } ) {
    super(data);
    this.responseSubType = responseSubType;
    this.responseType = responseType;
    this._responseConstructor = responseConstructor;
  }

  setCompletion(callback: (response: KontorolResponse<T>) => void): this {
    this.callback = callback;
    return this;
  }

  private _unwrapResponse(response: any): any {
    if (environment.response.nestedResponse) {
      if (response && response.hasOwnProperty('result')) {
        if (response.result.hasOwnProperty('error')) {
          return response.result.error;
        } else {
          return response.result;
        }
      } else if (response && response.hasOwnProperty('error')) {
        return response.error;
      }
    }

    return response;
  }

  parseServerResponse(response: any): { status: boolean, response: any} {
    try {

      const unwrappedResponse = this._unwrapResponse(response);

      if (unwrappedResponse instanceof KontorolAPIException) {
        // handle situation when multi request propagated actual api exception object.
        return { status: false, response: unwrappedResponse};
      }

      if (unwrappedResponse && unwrappedResponse.objectType === 'KontorolAPIException') {
        return { status: false,
          response: new KontorolAPIException(
            unwrappedResponse.message,
            unwrappedResponse.code,
            unwrappedResponse.args
          )};
      }

      const parsedResponse = unwrappedResponse ? super._parseResponseProperty(
        "",
        {
          type: this.responseType,
          subType: this.responseSubType
        },
        unwrappedResponse
      ) : undefined;

      if (!parsedResponse && this.responseType !== 'v') {
        return {
          status: false,
          response: new KontorolAPIException(`server response is undefined, expected '${this.responseType} / ${this.responseSubType}'`, 'client::response_type_error', null)
        };
      }

      return { status: true, response: parsedResponse};
    } catch (ex) {
      return {
        status: false,
        response: new KontorolAPIException(ex.message, 'client::general_error', null)
      };
    }
  }

  handleResponse(response: any, returnRawResponse: boolean = false): KontorolResponse<T> {
    let responseResult: any;
    let responseError: any;

    let result: KontorolResponse<T>;

    if (returnRawResponse) {
      result = new KontorolResponse(response, undefined);
    } else {
      let parsedResponse = this.parseServerResponse(response);

      result = parsedResponse.status ?
        new KontorolResponse<T>(parsedResponse.response, undefined) :
        new KontorolResponse<T>(undefined, parsedResponse.response);
    }

    if (this.callback) {
      try {
        this.callback(result);
      } catch (ex) {
        // do nothing by design
      }
    }

    return result;
  }

  setRequestOptions(optionArgs: KontorolRequestOptionsArgs): this;
  setRequestOptions(options: KontorolRequestOptions): this;
  setRequestOptions(arg: KontorolRequestOptionsArgs | KontorolRequestOptions): this {
    this.__requestOptions__ = arg instanceof KontorolRequestOptions ? arg : new KontorolRequestOptions(arg);
    return this;
  }

  getRequestOptions(): KontorolRequestOptions {
    return this.__requestOptions__;
  }

  buildRequest(defaultRequestOptions: KontorolRequestOptions | null, clientTag?: string): {} {
    const requestOptionsObject = this.__requestOptions__ ? this.__requestOptions__.toRequestObject() : {};
    const defaultRequestOptionsObject = defaultRequestOptions ? defaultRequestOptions.toRequestObject() : {};

    const result = Object.assign(
      {},
      defaultRequestOptionsObject,
      requestOptionsObject,
      this.toRequestObject()
    );

    if (environment.request.avoidQueryString) {
      result['clientTag'] = createClientTag(this, clientTag);
    }

    return result;

  }
}
