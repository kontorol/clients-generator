package com.kontorol.client.utils.response.base;

import com.kontorol.client.utils.response.OnCompletion;

/**
 * Created by tehila.rozin on 7/27/17.
 */

public interface ApiCompletion<T> extends OnCompletion<Response<T>> {
}
