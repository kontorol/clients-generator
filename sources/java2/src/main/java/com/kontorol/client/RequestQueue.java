package com.kontorol.client;

import com.kontorol.client.utils.request.ConnectionConfiguration;
import com.kontorol.client.utils.request.RequestElement;
import com.kontorol.client.utils.response.base.Response;

public interface RequestQueue {

    void setDefaultConfiguration(ConnectionConfiguration config);

    @SuppressWarnings("rawtypes")
	String queue(RequestElement request);

    @SuppressWarnings("rawtypes")
	Response<?> execute(RequestElement request);

    void cancelRequest(String reqId);

    void clearRequests();

    boolean isEmpty();

    void enableLogs(boolean enable);

    void enableLogResponseHeader(String header, boolean log);
}
