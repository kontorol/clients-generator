// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================
/*
 Forward declarations
 */
@class KontorolLibXmlWrapper;
@class KontorolXmlParserBase;
@class KontorolObjectBase;
@class KontorolException;

/*
 Protocol KontorolXmlParserDelegate
 */
@protocol KontorolLibXmlWrapperDelegate <NSObject>

@optional

- (void)parser:(KontorolLibXmlWrapper *)aParser didStartElement:(NSString *)aElementName;
- (void)parser:(KontorolLibXmlWrapper *)aParser didEndElement:(NSString *)aElementName;
- (void)parser:(KontorolLibXmlWrapper *)aParser foundCharacters:(NSString *)aString;
- (void)parser:(KontorolLibXmlWrapper *)aParser parseErrorOccurred:(NSError *)aParseError;

@end

/*
 Class KontorolLibXmlWrapper
 */
@interface KontorolLibXmlWrapper : NSObject
{
    struct _xmlParserCtxt* _xmlCtx;
    NSMutableString* _foundChars;
}

@property (nonatomic, assign) id<KontorolLibXmlWrapperDelegate> delegate;

- (void)processData:(NSData*)aData;
- (void)noMoreData;

@end

/*
 Protocol KontorolXmlParserDelegate
 */
@protocol KontorolXmlParserDelegate <NSObject>

- (void)parsingFinished:(KontorolXmlParserBase*)aParser;
- (void)parsingFailed:(KontorolXmlParserBase*)aParser;

@end

/*
 Class KontorolXmlParserBase
 */
@interface KontorolXmlParserBase : NSObject <KontorolLibXmlWrapperDelegate>
{
    id <KontorolLibXmlWrapperDelegate> _origDelegate;
    BOOL _attached;
}

@property (nonatomic, retain) KontorolLibXmlWrapper* parser;
@property (nonatomic, assign) id <KontorolXmlParserDelegate> delegate;
@property (nonatomic, retain) NSError* error;

- (void)attachToParser:(KontorolLibXmlWrapper*)aParser withDelegate:(id <KontorolXmlParserDelegate>)aDelegate;
- (void)detach;
- (void)callDelegateAndDetach;
- (void)parsingFailed:(KontorolXmlParserBase*)aParser;
- (id)result;

@end

/*
 Class KontorolXmlParserSkipTag
 */
@interface KontorolXmlParserSkipTag : KontorolXmlParserBase
{
    int _level;
}
@end

/*
 Class KontorolXmlParserSimpleType
 */
@interface KontorolXmlParserSimpleType : KontorolXmlParserBase
{
    NSString* _value;
}
@end

/*
 Class KontorolXmlParserException
 */
@interface KontorolXmlParserException : KontorolXmlParserBase <KontorolXmlParserDelegate>
{
    KontorolXmlParserBase* _subParser;
    KontorolXmlParserBase* _excObjParser;
    KontorolException* _targetException;
}

- (id)initWithSubParser:(KontorolXmlParserBase*)aSubParser;

@end

/*
 Class KontorolXmlParserObject
 */
@interface KontorolXmlParserObject : KontorolXmlParserBase <KontorolXmlParserDelegate>
{
    KontorolXmlParserBase* _subParser;
    KontorolObjectBase* _targetObj;
    NSString* _expectedType;
    NSString* _lastTagCapitalized;
    BOOL _lastIsObjectType;
    int _lastPropType;     // KontorolFieldType
}

- (id)initWithObject:(KontorolObjectBase*)aObject;
- (id)initWithExpectedType:(NSString*)aExpectedType;

@end

/*
 Class KontorolXmlParserArray
 */
@interface KontorolXmlParserArray : KontorolXmlParserBase <KontorolXmlParserDelegate>
{
    KontorolXmlParserBase* _subParser;
    NSString* _expectedType;
    NSMutableArray* _targetArr;
}

- (id)initWithExpectedType:(NSString*)aExpectedType;

@end

/*
 Class KontorolXmlParserMultirequest
 */
@interface KontorolXmlParserMultirequest : KontorolXmlParserBase <KontorolXmlParserDelegate>
{
    NSMutableArray* _subParsers;
    int _reqIndex;
}

- (void)addSubParser:(KontorolXmlParserBase*)aParser;
- (int)reqCount;

@end

/*
 Class KontorolXmlParserSkipPath
 */
@interface KontorolXmlParserSkipPath : KontorolXmlParserBase <KontorolXmlParserDelegate>
{
    KontorolXmlParserBase* _subParser;
    NSArray* _path;
    int _pathPosition;
    int _skipLevel;
}

- (id)initWithSubParser:(KontorolXmlParserBase*)aSubParser withPath:(NSArray*)aPath;

@end
