// ===================================================================================================
//                           _  __     _ _
//                          | |/ /__ _| | |_ _  _ _ _ __ _
//                          | ' </ _` | |  _| || | '_/ _` |
//                          |_|\_\__,_|_|\__|\_,_|_| \__,_|
//
// This file is part of the Kontorol Collaborative Media Suite which allows users
// to do with audio, video, and animation what Wiki platfroms allow them to do with
// text.
//
// Copyright (C) 2006-2011  Kontorol Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// @ignore
// ===================================================================================================
#import <Foundation/Foundation.h>
#import "ASIHTTPRequestDelegate.h"
#import "ASIProgressDelegate.h"
#import "KontorolXmlParsers.h"

/*
 Constants
 */
#define KONTOROL_UNDEF_BOOL      ([KontorolBool UNDEF_VALUE])
#define KONTOROL_UNDEF_INT       INT_MIN
#define KONTOROL_UNDEF_FLOAT     NAN
#define KONTOROL_UNDEF_STRING    (nil)
#define KONTOROL_UNDEF_OBJECT    (nil)

#define KONTOROL_NULL_BOOL   ([KontorolBool NULL_VALUE])
#define KONTOROL_NULL_INT    INT_MAX
#define KONTOROL_NULL_FLOAT  INFINITY
#define KONTOROL_NULL_STRING (@"__null_string__")
#define KONTOROL_NULL_OBJECT ([[[KontorolObjectBase alloc] init] autorelease])

extern NSString* const KontorolClientErrorDomain;

typedef int KONTOROL_BOOL;

typedef enum {
    KontorolClientErrorAPIException = 1,
    KontorolClientErrorInvalidHttpCode = 2,
    KontorolClientErrorUnknownObjectType = 3,
    KontorolClientErrorXmlParsing = 4,
    KontorolClientErrorUnexpectedTagInSimpleType = 5,
    KontorolClientErrorUnexpectedArrayTag = 6,
    KontorolClientErrorUnexpectedMultiReqTag = 7,
    KontorolClientErrorMissingMultiReqItems = 8,
    KontorolClientErrorMissingObjectTypeTag = 9,
    KontorolClientErrorExpectedObjectTypeTag = 10,
    KontorolClientErrorExpectedPropertyTag = 11,
    KontorolClientErrorStartTagInSimpleType = 12,
    KontorolClientErrorEmptyObject = 13,
} KontorolClientErrorType;

typedef enum 
{
    KFT_Invalid,
    KFT_Bool,
    KFT_Int,
    KFT_Float,
    KFT_String,
    KFT_Object,
    KFT_Array,
	KFT_Dictionary,
} KontorolFieldType;

/*
 Forward declarations
 */
@protocol KontorolXmlParserDelegate;
@class ASIFormDataRequest;
@class KontorolXmlParserBase;
@class KontorolLibXmlWrapper;
@class KontorolParams;
@class KontorolClientBase;

/*
 Class KontorolBool
 */
@interface KontorolBool : NSObject
+ (KONTOROL_BOOL)NO_VALUE;
+ (KONTOROL_BOOL)YES_VALUE;
+ (KONTOROL_BOOL)NULL_VALUE;
+ (KONTOROL_BOOL)UNDEF_VALUE;
@end

/*
 Class KontorolClientException
 */
@interface KontorolClientException : NSException
@end

/*
 Class KontorolSimpleTypeParser
 */
@interface KontorolSimpleTypeParser : NSObject

+ (KONTOROL_BOOL)parseBool:(NSString*)aStr;
+ (int)parseInt:(NSString*)aStr;
+ (double)parseFloat:(NSString*)aStr;

@end

/*
 Class KontorolObjectBase
 */
@interface KontorolObjectBase : NSObject

- (void)toParams:(KontorolParams*)aParams isSuper:(BOOL)aIsSuper;

@end

/*
 Class KontorolException
 */
@interface KontorolException : KontorolObjectBase

@property (nonatomic, copy) NSString* code;
@property (nonatomic, copy) NSString* message;

- (NSError*)error;

@end

/*
 Class KontorolObjectFactory
 */
@interface KontorolObjectFactory : NSObject

+ (KontorolObjectBase*)createByName:(NSString*)aName withDefaultType:(NSString*)aDefaultType;

@end

/*
 Class KontorolParams
 */
@interface KontorolParams : NSObject
{
    NSMutableArray* _params;
    NSMutableArray* _files;
    NSMutableString* _prefix;
}

- (void)setPrefix:(NSString*)aPrefix;
- (NSString*)get:(NSString*)aKey;
- (void)putKey:(NSString*)aKey withString:(NSString*)aVal;
- (void)putNullKey:(NSString*)aKey;
- (void)addIfDefinedKey:(NSString*)aKey withFileName:(NSString*)aFileName;
- (void)addIfDefinedKey:(NSString*)aKey withBool:(KONTOROL_BOOL)aVal;
- (void)addIfDefinedKey:(NSString*)aKey withInt:(int)aVal;
- (void)addIfDefinedKey:(NSString*)aKey withFloat:(double)aVal;
- (void)addIfDefinedKey:(NSString*)aKey withString:(NSString*)aVal;
- (void)addIfDefinedKey:(NSString*)aKey withObject:(KontorolObjectBase*)aVal;
- (void)addIfDefinedKey:(NSString*)aKey withArray:(NSArray*)aVal;
- (void)addIfDefinedKey:(NSString*)aKey withDictionary:(NSDictionary*)aVal;
- (void)sign;
- (void)addToRequest:(ASIFormDataRequest*)aRequest;
- (void)appendQueryString:(NSMutableString*)output;

@end

/*
 Protocol KontorolLogger
 */
@protocol KontorolLogger <NSObject>

- (void)logMessage:(NSString*)aMsg;

@end

/*
 Class KontorolNSLogger
 */
@interface KontorolNSLogger: NSObject <KontorolLogger>

@end

/*
 Class KontorolServiceBase
 */
@interface KontorolServiceBase : NSObject

@property (nonatomic, assign) KontorolClientBase* client;

- (id)initWithClient:(KontorolClientBase*)aClient;

@end

/*
 Class KontorolClientPlugin
 */
@interface KontorolClientPlugin : NSObject
@end

/*
 Class KontorolConfiguration
 */
@interface KontorolConfiguration : NSObject

@property (nonatomic, copy) NSString* serviceUrl;
@property (nonatomic, copy) NSString* clientTag;
@property (nonatomic, assign) int partnerId;
@property (nonatomic, assign) int requestTimeout;
@property (nonatomic, retain) id<KontorolLogger> logger;
@property (nonatomic, copy) NSDictionary* requestHeaders;

@end

/*
 Protocol KontorolClientDelegate
 */
@protocol KontorolClientDelegate

- (void)requestFinished:(KontorolClientBase*)aClient withResult:(id)result;
- (void)requestFailed:(KontorolClientBase*)aClient;

@end

/*
 Class KontorolClientBase
 */
@interface KontorolClientBase : NSObject <ASIHTTPRequestDelegate, KontorolXmlParserDelegate>
{
    BOOL _isMultiRequest;
    KontorolXmlParserBase* _reqParser;
    KontorolXmlParserBase* _skipParser;
    ASIFormDataRequest *_request;
    KontorolLibXmlWrapper* _xmlParser;
    NSDate* _apiStartTime;
}

@property (nonatomic, retain) KontorolConfiguration* config;
@property (nonatomic, retain) NSError* error;
@property (nonatomic, assign) id<KontorolClientDelegate> delegate;
@property (nonatomic, assign) id<ASIProgressDelegate> uploadProgressDelegate;
@property (nonatomic, assign) id<ASIProgressDelegate> downloadProgressDelegate;
@property (nonatomic, copy) NSString* ks;
@property (nonatomic, copy) NSString* apiVersion;
@property (nonatomic, readonly) KontorolParams* params;
@property (nonatomic, readonly) NSDictionary* responseHeaders;

    // public messages
- (id)initWithConfig:(KontorolConfiguration*)aConfig;
- (void)startMultiRequest;
- (NSArray*)doMultiRequest;
- (void)cancelRequest;
+ (NSString*)generateSessionWithSecret:(NSString*)aSecret withUserId:(NSString*)aUserId withType:(int)aType withPartnerId:(int)aPartnerId withExpiry:(int)aExpiry withPrivileges:(NSString*)aPrivileges;

    // messages for use of auto-gen service code
- (NSString*)queueServeService:(NSString*)aService withAction:(NSString*)aAction;
- (void)queueVoidService:(NSString*)aService withAction:(NSString*)aAction;
- (KONTOROL_BOOL)queueBoolService:(NSString*)aService withAction:(NSString*)aAction;
- (int)queueIntService:(NSString*)aService withAction:(NSString*)aAction;
- (double)queueFloatService:(NSString*)aService withAction:(NSString*)aAction;
- (NSString*)queueStringService:(NSString*)aService withAction:(NSString*)aAction;
- (id)queueObjectService:(NSString*)aService withAction:(NSString*)aAction withExpectedType:(NSString*)aExpectedType;
- (NSMutableArray*)queueArrayService:(NSString*)aService withAction:(NSString*)aAction withExpectedType:(NSString*)aExpectedType;

@end
