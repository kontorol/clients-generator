//
//  MediaInfoViewController_iPhone.h
//  Kontorol
//
//  Created by Pavel on 29.02.12.
//  Copyright (c) 2012 Kontorol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <KONTOROLPlayerSDK/KPViewController.h>

@class AppDelegate_iPhone;

extern const CGRect PlayerCGRect;

@interface MediaInfoViewController_iPhone : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate> {
    
    AppDelegate_iPhone *app;
    
    KontorolMediaEntry *mediaEntry;
    
    IBOutlet UIScrollView *scrollMain;
    
    IBOutlet UIView *viewIntro;
    IBOutlet UIView *viewDescription;

    IBOutlet KontorolThumbView *imgThumb;
    IBOutlet UILabel *labelTitle;

    IBOutlet UILabel *labelVTitle;
    IBOutlet UILabel *labelVDuration;

    IBOutlet UILabel *textDescription;
    
    IBOutlet UIButton *buttonPlay;
    IBOutlet UIButton *buttonCategory;
    
    IBOutlet UIView *viewShare;
    
    NSString *categoryName;

    KPViewController* playerViewController;
        
}

- (IBAction)menuBarButtonPressed:(UIButton *)button;
- (IBAction)categoryBarButtonPressed:(UIButton *)button;
- (IBAction)playButtonPressed;
- (IBAction)shareButtonPressed:(UIButton *)button;

// Supporting PlayerSDK 
- (void)stopAndRemovePlayer;
- (void)toggleFullscreen:(NSNotification *)note;

@property (nonatomic, retain) KontorolMediaEntry *mediaEntry;
@property (nonatomic, retain) NSString *categoryName;

@end
