//
//  MovieTableViewCell_iPad.h
//  Kontorol
//
//  Created by Pavel on 14.03.12.
//  Copyright (c) 2012 Kontorol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieTableViewCell_iPad : UITableViewCell {

    
    IBOutlet UIView *cell1View;
    IBOutlet KontorolThumbView *cell1Image;
    IBOutlet UILabel *cell1Label1;
    IBOutlet UILabel *cell1Label2;
    
    IBOutlet UIView *cell2View;
    IBOutlet KontorolThumbView *cell2Image;
    IBOutlet UILabel *cell2Label1;
    IBOutlet UILabel *cell2Label2;
    
    IBOutlet UIView *cell3View;
    IBOutlet KontorolThumbView *cell3Image;
    IBOutlet UILabel *cell3Label1;
    IBOutlet UILabel *cell3Label2;
    
    IBOutlet UIView *cell4View;
    IBOutlet KontorolThumbView *cell4Image;
    IBOutlet UILabel *cell4Label1;
    IBOutlet UILabel *cell4Label2;
    
    int index;
    
    UIViewController *parentController;
    
}

- (void)updateCell1:(KontorolMediaEntry *)mediaEntry;
- (void)updateCell2:(KontorolMediaEntry *)mediaEntry;
- (void)updateCell3:(KontorolMediaEntry *)mediaEntry;
- (void)updateCell4:(KontorolMediaEntry *)mediaEntry;

- (IBAction)selectCellView:(UIButton *)button;

@property (nonatomic, retain) IBOutlet UIView *cell1View;
@property (nonatomic, retain) IBOutlet UIView *cell2View;
@property (nonatomic, retain) IBOutlet UIView *cell3View;
@property (nonatomic, retain) IBOutlet UIView *cell4View;
@property int index;
@property (nonatomic, retain) UIViewController *parentController;

@end
