//
//  KontorolThumbView.h
//  Kontorol
//
//  Created by Pavel on 02.04.12.
//  Copyright (c) 2012 Kontorol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KontorolThumbView : UIImageView {
    
    KontorolMediaEntry *mediaEntry;
    
    int width;
    int height;
    
    BOOL isLoading;
    ASIHTTPRequest *request;
}

- (void)updateWithMediaEntry:(KontorolMediaEntry *)_mediaEntry;
- (void)updateWithMediaEntry:(KontorolMediaEntry *)_mediaEntry withSize:(CGSize)size;

@property (nonatomic, assign) KontorolMediaEntry *mediaEntry;

@end
