require 'rake'

spec = Gem::Specification.new do |s| 
  s.name = "kontorol-client"
  s.version = "@VERSION@"
  s.license = "AGPL-3.0"
  s.date = '2012-04-16'
  s.author = "Kontorol Inc."
  s.email = "community@kontorol.com"
  s.homepage = "http://www.kontorol.com/"
  s.summary = "A gem implementation of Kontorol's Ruby Client"
  s.description = "A gem implementation of Kontorol's Ruby Client."
  s.files = FileList["lib/**/*.rb","Rakefile","README", "agpl.txt", "kontorol.yml"].to_a
  s.test_files = FileList["{test}/test_helper.rb", "{test}/**/*test.rb", "{test}/media/*"].to_a
  s.add_dependency('rest-client')
end
