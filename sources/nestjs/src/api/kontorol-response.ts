import { KontorolAPIException } from './kontorol-api-exception';

export class KontorolResponse<T> {
  constructor(public result: T, public error: KontorolAPIException, public debugInfo?) {

  }
}
