export class KontorolAPIException {
  constructor(public message: string, public code: string, public args?: any) {
  }
}
