import { KontorolRequest, KontorolRequestArgs } from './kontorol-request';
import { environment } from '../environment';

// tslint:disable-next-line:no-empty-interface
export interface KontorolFileRequestArgs extends KontorolRequestArgs {
}

export class KontorolFileRequest extends KontorolRequest<{ url: string }> {

  constructor(data: KontorolFileRequestArgs) {
    super(data, { responseType: 'v', responseSubType: '', responseConstructor: null });
  }

  public getFormatValue() {
    return environment.request.fileFormatValue;
  }
}
