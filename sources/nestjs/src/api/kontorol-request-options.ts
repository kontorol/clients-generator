
import { KontorolObjectMetadata } from './kontorol-object-base';
import { KontorolBaseResponseProfile } from './types/KontorolBaseResponseProfile';
import { KontorolSkipCondition } from './types/KontorolSkipCondition';
import { KontorolObjectBase, KontorolObjectBaseArgs } from './kontorol-object-base';

export interface KontorolRequestOptionsArgs extends KontorolObjectBaseArgs {
  acceptedTypes?: { new(...args: any[]): KontorolObjectBase }[];
  partnerId?: number;
  userId?: number;
  language?: string;
  currency?: string;
  ks?: string;
  responseProfile?: KontorolBaseResponseProfile;
  abortOnError?: boolean;
  abortAllOnError?: boolean;
  skipCondition?: KontorolSkipCondition;
}

export class KontorolRequestOptions extends KontorolObjectBase {

  acceptedTypes: { new(...args: any[]): KontorolObjectBase }[];
  partnerId: number;
  userId: number;
  language: string;
  currency: string;
  ks: string;
  responseProfile: KontorolBaseResponseProfile;
  abortOnError: boolean;
  abortAllOnError: boolean;
  skipCondition: KontorolSkipCondition;

  constructor(data?: KontorolRequestOptionsArgs) {
    super(data);
    if (typeof this.acceptedTypes === 'undefined') {
      this.acceptedTypes = [];
    }
  }

  protected _getMetadata(): KontorolObjectMetadata {
    const result = super._getMetadata();
    Object.assign(
      result.properties,
      {
        partnerId: { type: 'n' },
        userId: { type: 'n' },
        language: { type: 's' },
        currency: { type: 's' },
        ks: { type: 's' },
        responseProfile: { type: 'o', subTypeConstructor: KontorolBaseResponseProfile, subType: 'KontorolBaseResponseProfile' },
        abortOnError: { type: 'b' },
        abortAllOnError: { type: 'b' },
        skipCondition: { type: 'o', subTypeConstructor: KontorolSkipCondition, subType: 'KontorolSkipCondition' },
      },
    );
    return result;
  }
}
