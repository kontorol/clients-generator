export const KONTOROL_CLIENT_OPTIONS = 'KONTOROL_CLIENT_OPTIONS';
export const KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS = 'KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS';
export interface KontorolClientOptions {
  clientTag: string;
  endpointUrl: string;
  chunkFileSize?: number;
  chunkFileDisabled?: boolean;
}
