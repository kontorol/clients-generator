import { map, catchError } from 'rxjs/operators';
import { KontorolRequest } from '../api/kontorol-request';
import { Observable } from 'rxjs';

import { KontorolAPIException } from '../api/kontorol-api-exception';
import { KontorolClientException } from '../api/kontorol-client-exception';
import { KontorolRequestOptions } from '../api/kontorol-request-options';
import { KontorolClientOptions } from '../kontorol-client-options';
import { createEndpoint, getHeaders, prepareParameters } from './utils';
import { environment } from '../environment';
import { HttpService } from '@nestjs/common';
import { AxiosRequestConfig } from 'axios';

export class KontorolRequestAdapter {

  constructor(private http: HttpService) {
  }

  public transmit<T>(request: KontorolRequest<T>, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions): Observable<T>;
  public transmit<T>(request: KontorolRequest<any>, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions, format: string): Observable<any>;
  public transmit<T>(request: KontorolRequest<any>, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions, format: string, responseType: 'blob' | 'text'): Observable<any>;
  public transmit<T>(request: KontorolRequest<T>, clientOptions: KontorolClientOptions, defaultRequestOptions: KontorolRequestOptions, format?: string, responseType: 'blob' | 'text' = 'text'): Observable<any> {

    const requestSpecificFormat = typeof format !== 'undefined';
    const parameters = prepareParameters(request, clientOptions, defaultRequestOptions);

    const endpointOptions = { ...clientOptions, service: parameters['service'], action: parameters['action'], format };
    const endpointUrl = createEndpoint(request, endpointOptions);
    delete parameters['service'];
    delete parameters['action'];

    const config: AxiosRequestConfig = {
      url: endpointUrl,
      method: 'POST',
      data: parameters,
      responseType: requestSpecificFormat ? responseType || 'text' : 'json',
      headers: requestSpecificFormat ? undefined : getHeaders(),
    };
    return this.http.request(config)
      .pipe(
        catchError(
          error => {
            if (environment.response.customErrorInHttp500) {
              if (error && typeof error.error === 'string') {
                const actualError = JSON.parse(error.error).result.error;
                throw new KontorolAPIException(actualError.message, actualError.code, actualError.args);
              }
              if (error && error.error) {
                return Observable.create((observer) => {
                  const reader = new FileReader();
                  reader.addEventListener('loadend', (e) => {
                    const text = (e.srcElement as any).result;
                    const actualError = JSON.parse(text).result.error;
                    observer.error(new KontorolAPIException(actualError.message, actualError.code, actualError.args));
                  });

                  // Start reading the blob as text.
                  reader.readAsText(error.error);
                });
              }
            }

            const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
            throw new KontorolClientException('client::request-network-error', errorMessage || 'Error connecting to server');
          },
        ),
        map(
          result => {
            try {
              const response = request.handleResponse(result, requestSpecificFormat);

              if (response.error) {
                throw response;
              } else {
                return response;
              }
            } catch (error) {

              if (error.hasOwnProperty('error') && error.error instanceof KontorolAPIException) {
                throw error;
              }
              if (error instanceof KontorolClientException || error instanceof KontorolAPIException) {
                throw error;
              } else {
                const errorMessage = error instanceof Error ? error.message : typeof error === 'string' ? error : null;
                throw new KontorolClientException('client::response-unknown-error', errorMessage || 'Failed to parse response');
              }
            }
          }));
  }
}
