import { KontorolClient } from './kontorol-client.service';

import { KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS, KONTOROL_CLIENT_OPTIONS, KontorolClientOptions } from './kontorol-client-options';
import { KontorolRequestOptionsArgs } from './api/kontorol-request-options';
import { DynamicModule, HttpModule, Module, Optional } from '@nestjs/common';

@Module({
  imports: [
    HttpModule,
  ],
  exports: [],
  providers: [],
})
export class NestJsClientModule {

  constructor(@Optional() module: NestJsClientModule) {
    if (module) {
      throw new Error('\'KontorolClientModule\' module imported twice.');
    }
  }

  static forRoot(clientOptionsFactory?: () => KontorolClientOptions,
                 defaultRequestOptionsArgsFactory?: () => KontorolRequestOptionsArgs): DynamicModule {
    return {
      module: NestJsClientModule,
      providers: [
        KontorolClient,
        {
          provide: KONTOROL_CLIENT_OPTIONS,
          useFactory: clientOptionsFactory,
        },
        {
          provide: KONTOROL_CLIENT_DEFAULT_REQUEST_OPTIONS,
          useFactory: defaultRequestOptionsArgsFactory,
        },
      ],
      exports: [KontorolClient],
    };
  }
}
